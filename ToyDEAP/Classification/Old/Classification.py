import ROOT
import numpy as np
import math as math
import os
import linecache
import sys
import random


# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return np.sqrt(x)
# Redefine pi to something shorter
pi = np.pi

#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();
print ''





### Inspired by DEAP-3600 surface background problem.
# I've done this before via GEANT4 Monte Carlo (and some real data), but let's try real kNN with a toy model:
# 2D only.
# I. For surface background events: DONE
#   i. Generate events (uniformly) on a circle (MC true surface). DONE
#   ii. Randomly move them around, preferably preferentially towards the center (Reconstruction) DONE
# II. For bulk events: DONE
#   i. Generate events (uniformly) on disk (MC true bulk). DONE
#   ii. Optional. Randomly move them around. This doesn't really effect much, but is akin to more reconstruction analysis. DONE
# III. With this training data, use kNN to parse these points into surface/bulk.
# IV. Generate new data for it try.

Directory = 'TrainingData/'
FileMCS = 'MonteCarlo_Surf'
FileMCB = 'MonteCarlo_Bulk'
FileRS = 'Reconstruction_Surf'
FileRB = 'Reconstruction_Bulk'
Extension = '.txt'


R = 1.0


### Get reconstructed surface events
xRS, yRS = np.genfromtxt(Directory+FileRS+Extension, delimiter=',', unpack=True)

### Graph reconstructed surface events
gRS = ROOT.TGraph(len(xRS), np.array(xRS), np.array(yRS))
gRS.GetXaxis().SetLimits(-R, R)
gRS.GetYaxis().SetRangeUser(-R, R)
gRS.SetMarkerStyle(ROOT.kFullDotLarge)
gRS.SetMarkerColor(ROOT.kGray+2)
gRS.SetMarkerSize(0.25)


### Get reconstructed bulk events
xRB, yRB = np.genfromtxt(Directory+FileRB+Extension, delimiter=',', unpack=True)

### Graph reconstructed surface events
gRB = ROOT.TGraph(len(xRB), np.array(xRB), np.array(yRB))
gRB.GetXaxis().SetLimits(-R, R)
gRB.GetYaxis().SetRangeUser(-R, R)
gRB.SetMarkerStyle(ROOT.kFullDotLarge)
gRB.SetMarkerColor(ROOT.kViolet+1)
gRB.SetMarkerSize(0.25)


### Plot data
c = ROOT.TCanvas('c', 'c', 720, 720)
gRB.SetTitle('nRB='+str(len(xRB))+', nRS='+str(len(xRS))+';;')
gRB.Draw('ap')
gRS.Draw('same p')






