### From http://machinelearningmastery.com/tutorial-to-implement-k-nearest-neighbors-in-python-from-scratch/
# Example of kNN implemented from Scratch in Python


# Edited to take ToyDEAP3600 data, which I put to file in same manner as iris.data. These two bits were hardcoded...
# Takes data in the form [0.0763758342428, -0.377705472767, bulk] where nDatacolumns is 2 here. Third is event ID.
'''
Function = 'Train'
#Function = 'Classify'

if Function == 'Train':
  DataDirectory = '../TrainingData/'
if Function == 'Classify':
  DataDirectory = '../TestingData/'

Filename = 'ToyDEAP3600.data'
nDataColumns = 2
'''



### Just copy, in own code, the iris.py code.
### Then instead of splitting single dataset, pull two files instead

import numpy as np
import random
from collections import Counter

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return np.sqrt(x)
# Redefine pi to something shorter
pi = np.pi












### Define data file, number of features, and data delimiter. Assumes n cols of feature doubles with n+1 col as classification string.
Filename = 'ToyDEAP3600.data'
nFeatures = 2
Delimiter = ','

### Use some percent of data if desired, e.g. 100/100. == 100% of data.
PercentOfData = 2/100.

### 67% of data to train model, last 33% to test model. 
Split = 2/3.

### k-nearest neighbors. How many nearest neighbors?
k = 3







### Load data, splitting data randomly into training or test sets.
def LoadData(filename, split, trainingset=[], testset=[]):
  #trainingset = []
  #testset = []
  data = np.genfromtxt(filename, delimiter=Delimiter, dtype=None)
  print 'Full dataset:', len(data)
  data = data[:int(len(data)*PercentOfData)]
  print 'Analyzed dataset:', len(data)

  #features = data[:-1]
  #classification = data[-1]
  for instance in data:
    if random.random() < split:
      trainingset.append(instance)
    else:
      testset.append(instance)
     
  print 'Training set:', len(trainingset)
  print 'Test set:', len(testset)

 

### Define Euclidean distance This distance-calculating method may vary (Manhattan, Hamming, ...), so define explicitly as such.
# r = sqrt( (x-x0)**2 + ... ) for 1D. Add (y-y0)**2 for 2D, etc.
def EuclideanDistance(instance1, instance2):#, nfeatures):
  #distance = 0
  #for i in range(nfeatures):
  #  distance = distance + (instance1[i]-instance2[i])**2
  #return sqrt(distance)

  # Do not include classification in calculations, so [:-1]. Only features.
  # Works without list(x) individually, but requires it when running whole program?
  #a = [1,2,3,'a']; b = [11,21,31,'b']; EuclideanDistance(a,b) = 35.284557528754704
  features1 = list(instance1)[:-1]
  features2 = list(instance2)[:-1]
  r = sqrt(sum([(f1-f2)**2 for f1,f2 in zip(features1, features2)]))
  return r


### Determine k neighbors. Take in full training dataset and single test instance. 
# Compute distances between single test instance and every training instance (this is looped from main() for every test instance).
# Take k smallest distances.
def GetNeighbors(trainingset, testinstance, k):
  distances = []
  for traininginstance in trainingset:
    distance = EuclideanDistance(traininginstance, testinstance)
    #print traininginstance, distance
    distances.append( (traininginstance, distance) )
    #print (traininginstance, distance)
  #neighbors = np.sort(distances)[:k]
  # distances has form ((-0.354481106014, -0.784429584872, 'bulk'), 0.611469612650144) = ((x,y,id),r). 
  # Sort by r. Idk how this works. Take only first k neighbors.
  neighbors_distances = sorted(distances, key=lambda r:r[:][-1])[:k]
  neighbors_nodistances = [i[0] for i in neighbors_distances]
  #print neighbors
  #print neighbors[:-1]
  return neighbors_nodistances

# distances = [((0.590625421675, -0.537336203196, 'surf'), 1.4323723100760823), ((0.596438163342, 0.382658483384, 'surf'), 1.6357006006854593), ((0.046935305266, -0.453563328116, 'bulk'), 0.8839805903449066), ((0.200010831516, -0.691165763804, 'surf'), 1.0740265539177047), ((0.559160345487, 0.554011998208, 'surf'), 1.6941735293559765), ((-0.354481106014, -0.784429584872, 'bulk'), 0.611469612650144)]
# for i in distances: print i
# [i[-1] for i in distances]




### Get response, i.e. vote. Assume classification is index [-1] of neighbors. Counts how many times each classification appears.
# neighbors = [[1,1,1,'a'], [2,2,2,'a'], [3,3,3,'b']]
# GetResponse(neighbors) 
def GetResponse(neighbors):
  #Classification = {} # what is this {}? Dict?
  #print neighbors
  #for i in neighbors:
    #print i
    #response = i[-1]
    #print response
    #if response in Classification:
      #Classification[response] = Classification[response] + 1
    #else:
      #Classification[response] = 1
  #SortedVotes = []  
  #print Classification[0][0]
  #return Classification[0][0]

  #response = [i[-1] for i in neighbors]
  #[i[-2][-1] for i in nn]
  responses = [i[-1] for i in neighbors]
  #print responses
  classification = Counter(responses).most_common(1)[0][0]
  #print classification
  return classification


### How accurate was response?
def GetAccuracy(testset, predictions):
  correct = 0
  for id,p in zip(testset, predictions):
    print id[-1],p
    if id[-1] == p:
      correct = correct + 1

  return (correct/float(len(testset)))*100.




# FYI, I think the primary benefit of putting all "running" code under a main() function is so one can import without "running".
# However, these variables are not saved outside, and cannot be called later for review.
def main():
  print ''
  print 'File:', Filename
  print 'Percent of file analyzed:', str(PercentOfData*100)+'%'
  print 'Split:', round(Split,2)
  print 'k:', k
  print ''

  TrainingSet = []
  TestSet = []
  LoadData(Filename, Split, TrainingSet, TestSet)

  Predictions = []
  for testinstance in TestSet:
    Neighbors = GetNeighbors(TrainingSet, testinstance, k)
    Result = GetResponse(Neighbors)
    Predictions.append(Result)
  Accuracy = GetAccuracy(TestSet, Predictions)

  print 'Accuracy:', str(Accuracy)+'%'




main()









'''

import csv
import random
import math
import operator


def loadDataset(filename, split, trainingSet=[] , testSet=[]):
	with open(filename, 'rb') as csvfile:
	    lines = csv.reader(csvfile)
	    dataset = list(lines)
	    for x in range(len(dataset)-1):
	        for y in range(nDataColumns):
	            dataset[x][y] = float(dataset[x][y])
	        if random.random() < split:
	            trainingSet.append(dataset[x])
	        else:
	            testSet.append(dataset[x])
 
 
def euclideanDistance(instance1, instance2, length):
	distance = 0
	for x in range(length):
		distance += pow((instance1[x] - instance2[x]), 2)
	return math.sqrt(distance)
 
def getNeighbors(trainingSet, testInstance, k):
	distances = []
	length = len(testInstance)-1
	for x in range(len(trainingSet)):
		dist = euclideanDistance(testInstance, trainingSet[x], length)
		distances.append((trainingSet[x], dist))
	distances.sort(key=operator.itemgetter(1))
	neighbors = []
	for x in range(k):
		neighbors.append(distances[x][0])
	return neighbors
 
def getResponse(neighbors):
	classVotes = {}
	for x in range(len(neighbors)):
		response = neighbors[x][-1]
		if response in classVotes:
			classVotes[response] += 1
		else:
			classVotes[response] = 1
	sortedVotes = sorted(classVotes.iteritems(), key=operator.itemgetter(1), reverse=True)
	return sortedVotes[0][0]
 
def getAccuracy(testSet, predictions):
	correct = 0
	for x in range(len(testSet)):
		if testSet[x][-1] == predictions[x]:
			correct += 1
	return (correct/float(len(testSet))) * 100.0
	
def main():
	# prepare data
	trainingSet=[]
	testSet=[]
	split = 0.67
	loadDataset(File, split, trainingSet, testSet)
	print 'Train set: ' + repr(len(trainingSet))
	print 'Test set: ' + repr(len(testSet))
	# generate predictions
	predictions=[]
	k = 3
	for x in range(len(testSet)):
		neighbors = getNeighbors(trainingSet, testSet[x], k)
		result = getResponse(neighbors)
		predictions.append(result)
		#print x,'of', len(testSet), ('> predicted=' + repr(result) + ', actual=' + repr(testSet[x][-1]))
	accuracy = getAccuracy(testSet, predictions)
	print('Accuracy: ' + repr(accuracy) + '%')
	
main()
'''
