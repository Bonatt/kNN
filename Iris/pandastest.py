import pandas as pd
import matplotlib.pyplot as plt


# From http://pandas.pydata.org/pandas-docs/stable/dsintro.html

print ''
print(plt.style.available)
plt.style.use('ggplot') 
print ''

# "Since a function is passed in, the function is computed on the DataFrame being assigned to. Importantly, this is the DataFrame that's been filtered to those rows with sepal length greater than 5. The filtering happens first, and then the ratio calculations. This is an example where we didn't have a reference to the filtered DataFrame available."

iris = pd.read_csv('iris.data', names = ['SepalLength', 'SepalWidth', 'PetalLength', 'PetalWidth', 'Name'])

iris.query('SepalLength > 5').assign(SepalRatio = lambda x: x.SepalWidth/x.SepalLength,\
                                     PetalRatio = lambda x: x.PetalWidth/x.PetalLength) \
                             .plot(kind='scatter',x='SepalRatio',y='PetalRatio')

plt.show()

