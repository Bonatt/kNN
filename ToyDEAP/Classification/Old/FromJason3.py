import ROOT
import numpy as np
import random
import math
from operator import itemgetter
import time
import csv
from collections import Counter


#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();


# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return math.sqrt(x)
# Redefine pi to something shorter
pi = math.pi





### From http://machinelearningmastery.com/tutorial-to-implement-k-nearest-neighbors-in-python-from-scratch/
# Edited to take ToyDEAP3600 data, which I put to file in same manner as iris.data. 
# Rerewriting this code because my first attempt to emulate this code "in my own words" results in worse timing: his ~6 s vs my ~42 s.
# See "FollowingJason" for that attempt.
# Takes data in the form [0.0763758342428, -0.377705472767, bulk] where nFeatures is 2 here. Third is event ID.
Filename = 'ToyDEAP_1923-2000.data'
#Filename = 'ToyDEAP_3855-4000.data'
#Filename = 'ToyDEAP_4830-5000.data'
#Filename = 'ToyDEAP_7710-8000.data'
#Filename = 'ToyDEAP_9636-10000.data'
#Filename = 'iris.data'

### How many columns of file are data? And use some percent of data if desired, e.g. 100/100. == 100% of data.
if 'ToyDEAP' in Filename:
  nFeatures = 2
  #PercentOfData = 100/100.
if 'iris' in Filename:
  nFeatures = 4
  #PercentOfData = 100/100.





### Load data, splitting data randomly into training or test sets. And make features from string to float
def LoadData(filename, split):
  # filename = str
  # split = float
  trainingset = []
  testset = []
  with open(filename, 'rb') as csvfile:
    dataset = list(csv.reader(csvfile))
    
    for row in dataset:
      for f in xrange(len(row)-1): #nFeatures):
        row[f] = float(row[f])

      if random.random() < split:
        trainingset.append(row)
      else:
        testset.append(row)

  return trainingset, testset





### Define Euclidean distance This distance-calculating method may vary (Manhattan, Hamming, ...), so define explicitly as such.
# r = sqrt( (x-x0)**2 + ... ) for 1D. Add (y-y0)**2 for 2D, etc.
def EuclideanDistance(instance1, instance2):
  # instance1, instance2 = [f1, f2, ..., 'id']
  distance = 0
  for f in xrange(nFeatures):
    distance += (instance1[f] - instance2[f])**2

  return distance #sqrt(distance)




 
### Determine k neighbors. Take in full training dataset and single test instance. 
# Compute distances between single test instance and every training instance (this is looped from main() for every test instance).
# Take k smallest distances.
def GetNeighbors(trainingset, testinstance, k):
  # trainingset = [ [f1, f2, ..., 'id'], [...] ]
  # testinstance = [f1, f2, ..., 'id']
  # k = int
  distances = []
  for traininginstance in trainingset:
    distance = EuclideanDistance(traininginstance, testinstance)
    distances.append((traininginstance, distance))
  distances.sort(key=itemgetter(1))
  ''' 
  neighbors = []
  for d in distances[:k]:
    neighbors.append(d[0])
  return neighbors
  '''
  '''
  neighbors = []
  weights = []
  for d in distances[:k]:
    neighbors.append(d[0])
    weights.append(distances[0][1]/d[1])
  return neighbors, weights 
  ''' 
  #'''
  neighbors = []
  ndistances = []
  for d in distances[:k]:
    neighbors.append(d[0])
    ndistances.append(d[1])
  return neighbors, ndistances
  #'''
  


### Get normalized weights, e.g. [6.01761e-05, 0.00133, 0.00173] --> [1.0, 0.04515, 0.03468]
def GetWeights(ndistances):
  # distances = [d1, d2, ..., dk]

  #print ndistances
  
  # Normalize closest d to 100, all others fall off via negative exponential
  #weights = [math.exp(-i)*100*ndistances[0]/d for i,d in enumerate(ndistances)]
  
  # Not normalization, just function of exponential. Larger factor means greater separation: 10 = 8.67 vs 8.78, 1000 = 887 vs 878.
  # Factor changes processing time. 100 ~5 s, same speed as without. 1000 ~7 s. 10000 ~30 s. AND accuracy goes down.
  #f = 100
  #weights = [f*math.exp(-d) for d in ndistances]

  # Just inverse of distance
  weights = [1/nd for nd in ndistances]

  return weights




### Get response, i.e. vote. Assume classification is index [-1] of neighbors. Counts how many times each classification appears. 
def GetResponse(neighbors):
  # neighbors =  [ [f1, f2, ..., 'id'], [...] ] 
  responses = [i[-1] for i in neighbors]

  return Counter(responses).most_common(1)[0][0]

### Get weighted response
def GetResponseWeighted(neighbors, weights):
  # neighbors =  [ [f1, f2, ..., 'id'], [...] ] 
  # weights = [w1, w2, ... wk]
  responses = [i[-1] for i in neighbors] #[i[-1]*w for i,w in zip(neighbors,weights)]

  responsesWeighted = []
  for r,w in zip(responses,weights):
    for i in xrange(int(w)):
      responsesWeighted.append(r)

  return Counter(responsesWeighted).most_common(1)[0][0]





### How accurate was response? This could be done in above loop...? 
def GetAccuracy(testset, predictions):
  # testset = [ [f1, f2, ..., 'id'], [...] ]
  # predictions = [ 'id', 'id', ... ] 
  Correct = []
  Incorrect = []
 
  correct = 0
  for t,p in zip(testset, predictions):
    if t[-1] == p:
      correct += 1
      Correct.append(t)
    else:
      Incorrect.append(t)

  return (correct/float(len(testset))) * 100.0, Correct, Incorrect

 


 
def main(k,split,weightboolean):
  start = time.clock()

  # Prepare data
  TrainingSet, TestSet = LoadData(Filename, Split)
  print ''
  print 'Data:', Filename
  #print 'Percent of data analyzed:', str(int(PercentOfData*100))+'%'
  print 'Data set:', len(TrainingSet+TestSet)
  print 'Split:', round(Split,2)
  print 'Training set:', len(TrainingSet)
  print 'Test set:', len(TestSet)
  print 'k:', k
  print 'Weight on?:', weightboolean

  # Generate predictions
  Predictions = []  
  for testinstance in TestSet:
    Neighbors, Distances = GetNeighbors(TrainingSet, testinstance, k)
    
    if weightboolean == 0:
      Response = GetResponse(Neighbors)
    else:
      Weights = GetWeights(Distances)
      Response = GetResponseWeighted(Neighbors, Weights)    

    Predictions.append(Response)


  Accuracy, Correct, Incorrect = GetAccuracy(TestSet, Predictions)
  print('Accuracy: ' + repr(round(Accuracy,2)) + '%')
  
  stop = time.clock()
  print 'Runtime:', stop-start, 's'
  print ''

  return TrainingSet, TestSet, Predictions, Correct, Incorrect



### Actually run program. I modified it to loop over values of k (, split).
# 67% of data to train model, last 33% to test model. 
Split = 2/3.

# k-nearest neighbors. How many nearest neighbors?
# From https://stackoverflow.com/questions/18110951/how-to-determine-k-value-for-the-k-nearest-neighbours-algorithm-for-a-matrix-in
if 'ToyDEAP' in Filename:
  k = int(round(sqrt(1846)))
if 'iris' in Filename:
  k = int(round(sqrt(150))) #3
#k = 3

# Use weighted distance vote?
WeightBoolean = 0 

# How many times to loop over main()?
Loops = 1 


#AccuracyRuntime = []
for m in xrange(Loops):
  TrainingSet, TestSet, Predictions, Correct, Incorrect = main(k,Split,WeightBoolean)


#import profile  
#profile.run('main()')

Accuracy = len(Correct)/float(len(TestSet))






### Want four plots:
# 1. Full dataset: training and test on same plot
# 2. Test set: predictions
# 3. Test set: actual
# 4. Test set: correct/incorrect as green/red.










R = 1.0





TrainingSetx = [i[0] for i in TrainingSet]
TrainingSety = [i[1] for i in TrainingSet]

TestSetx = [i[0] for i in TestSet]
TestSety = [i[1] for i in TestSet]

### Graph training set events
gTrainingSet = ROOT.TGraph(len(TrainingSet), np.array(TrainingSetx), np.array(TrainingSety))
gTrainingSet.GetXaxis().SetLimits(-R, R)
gTrainingSet.GetYaxis().SetRangeUser(-R, R)
#gTrainingSet.SetMarkerStyle(ROOT.kFullDotLarge)
#gTrainingSet.SetMarkerColor(ROOT.kGray+2) #9
gTrainingSet.SetMarkerSize(0.25)

### Graph test set events
gTestSet = ROOT.TGraph(len(TestSet), np.array(TestSetx), np.array(TestSety))
gTestSet.GetXaxis().SetLimits(-R, R)
gTestSet.GetYaxis().SetRangeUser(-R, R)
gTestSet.SetMarkerStyle(ROOT.kFullStar) #DotLarge)
#gTestSet.SetMarkerColor(ROOT.kOrange) #Violet+1)
gTestSet.SetMarkerSize(1)

### Plot full dataset
c = ROOT.TCanvas('c', 'Toy DEAP', 720, 720) #1366 
c.Divide(2,2)
c.cd(1)
gTestSet.SetTitle('Full: Training = '+str(len(TrainingSet))+', Test = '+str(len(TestSet))+';;')
gTestSet.Draw('ap')
gTrainingSet.Draw('same p')







TestBulk = []
TestBulkx = []
TestBulky = []

TestSurf = []
TestSurfx = []
TestSurfy = []

for t in TestSet:
  if 'bulk' in t:
    TestBulk.append(t)
    TestBulkx.append(t[0])
    TestBulky.append(t[1])
  if 'surf' in t:
    TestSurf.append(t)
    TestSurfx.append(t[0])
    TestSurfy.append(t[1])

### Graph true bulk events
gTestBulk = ROOT.TGraph(len(TestBulk), np.array(TestBulkx), np.array(TestBulky))
gTestBulk.GetXaxis().SetLimits(-R, R)
gTestBulk.GetYaxis().SetRangeUser(-R, R)
gTestBulk.SetMarkerStyle(ROOT.kFullStar) #DotLarge)
gTestBulk.SetMarkerColor(ROOT.kViolet+2)
gTestBulk.SetMarkerSize(1)

### Graph true surface events
gTestSurf = ROOT.TGraph(len(TestSurf), np.array(TestSurfx), np.array(TestSurfy))
gTestSurf.GetXaxis().SetLimits(-R, R)
gTestSurf.GetYaxis().SetRangeUser(-R, R)
gTestSurf.SetMarkerStyle(ROOT.kFullStar) #DotLarge)
gTestSurf.SetMarkerColor(ROOT.kGray+1)
gTestSurf.SetMarkerSize(1)

### Plot test bulk, surface events
#c2 = ROOT.TCanvas('c2', 'c2', 720, 720)
c.cd(2)
gTestBulk.SetTitle('True: Bulk = '+str(len(TestBulk))+', Surf = '+str(len(TestSurf))+';;')
gTestBulk.Draw('ap')
gTestSurf.Draw('same p')







PredBulk = []
PredBulkx = []
PredBulky = []

PredSurf = []
PredSurfx = []
PredSurfy = []

for p,t in zip(Predictions,TestSet):
  if 'bulk' in p:
    PredBulk.append(t)
    PredBulkx.append(t[0])
    PredBulky.append(t[1])
  if 'surf' in p:
    PredSurf.append(t)
    PredSurfx.append(t[0])
    PredSurfy.append(t[1])

### Graph predicted bulk events
gPredBulk = ROOT.TGraph(len(PredBulk), np.array(PredBulkx), np.array(PredBulky))
gPredBulk.GetXaxis().SetLimits(-R, R)
gPredBulk.GetYaxis().SetRangeUser(-R, R)
gPredBulk.SetMarkerStyle(ROOT.kFullStar) #DotLarge)
gPredBulk.SetMarkerColor(ROOT.kViolet+2)
gPredBulk.SetMarkerSize(1)

### Graph predicted surface events
gPredSurf = ROOT.TGraph(len(PredSurf), np.array(PredSurfx), np.array(PredSurfy))
gPredSurf.GetXaxis().SetLimits(-R, R)
gPredSurf.GetYaxis().SetRangeUser(-R, R)
gPredSurf.SetMarkerStyle(ROOT.kFullStar) #DotLarge)
gPredSurf.SetMarkerColor(ROOT.kGray+1)
gPredSurf.SetMarkerSize(1)

### Plot data
#c3 = ROOT.TCanvas('c3', 'c3', 720, 720)
c.cd(3)
gPredBulk.SetTitle('Prediction: Bulk = '+str(len(PredBulk))+', Surf = '+str(len(PredSurf))+', k = '+str(k)+';;')
gPredBulk.Draw('ap')
gPredSurf.Draw('same p')






Correctx = [i[0] for i in Correct]
Correcty = [i[1] for i in Correct]

Incorrectx = [i[0] for i in Incorrect]
Incorrecty = [i[1] for i in Incorrect]

### Graph correctly predicted events
gCorrect = ROOT.TGraph(len(Correct), np.array(Correctx), np.array(Correcty))
gCorrect.GetXaxis().SetLimits(-R, R)
gCorrect.GetYaxis().SetRangeUser(-R, R)
gCorrect.SetMarkerStyle(ROOT.kFullStar) #DotLarge)
gCorrect.SetMarkerColor(ROOT.kGreen)
gCorrect.SetMarkerSize(1)

### Graph incorrectly predicted events
gIncorrect = ROOT.TGraph(len(Incorrect), np.array(Incorrectx), np.array(Incorrecty))
gIncorrect.GetXaxis().SetLimits(-R, R)
gIncorrect.GetYaxis().SetRangeUser(-R, R)
gIncorrect.SetMarkerStyle(ROOT.kFullStar) #DotLarge)
gIncorrect.SetMarkerColor(ROOT.kRed)
gIncorrect.SetMarkerSize(1)

### Plot 
#c4 = ROOT.TCanvas('c4', 'c4', 720, 720)
c.cd(4)
gCorrect.SetTitle('Correct = '+str(len(Correct))+', Incorrect = '+str(len(Incorrect))+' ('+str(int(Accuracy*100))+'%);;')
gCorrect.Draw('ap')
gIncorrect.Draw('same p')







c.SaveAs('ToyDEAP.png')




