import ROOT
import numpy as np
import math as math
import os
import linecache
import sys
import random


# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return np.sqrt(x)
# Redefine pi to something shorter
pi = np.pi    

#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();
print ''




### Inspired by DEAP-3600 surface background problem.
# I've done this before via GEANT4 Monte Carlo (and some real data), but let's try real kNN with a toy model:
# 2D only.
# I. For surface background events:
#   i. Generate events (uniformly) on a circle (MC true surface).
#   ii. Randomly move them around, preferably preferentially towards the center (Reconstruction) 
# II. For bulk events:
#   i. Generate events (uniformly) on disk (MC true bulk).
#   ii. Optional. Randomly move them around. This doesn't really effect much, but is akin to more reconstruction analysis.
# III. With this training data, use kNN to parse these points into surface/bulk.
# IV. Generate new data for it try.
#
# V. Generate neck events.
#
# Much of this code was pulled from AvgDistInCircle
#



### DEAP-3600 parameters. May not be used much.
DEAPRadius = 0.85                                                       # m
DEAPSurfaceArea = 4.*pi*DEAPRadius**2                                # m2, roughly outside
DEAPVolume = 4./3.*pi*DEAPRadius**3                                  # m3
DEAPCircumference = 2.*pi*DEAPRadius





# Define unit circle parameters centered at (0,0).
R = 1.								# Radius of detector
x0 = 0.
y0 = 0.




###### Create various interesting aspects regarding detector ######

### Draw neck of detector
gNeck = ROOT.TGraph(1, np.array(R), np.array(y0))
gNeck.SetMarkerStyle(ROOT.kCircle)
gNeck.SetMarkerColor(ROOT.kGray+2)
gNeck.SetMarkerSize(1)

### Generate "PMT" locations on detector surface
nPMT = 19                                                       # Counted lightguides in standard cut drawing instead.
print ' ', nPMT, 'PMTs.'
# There used to be an offset here to simulate gap in PMTs at neck. It made centroid not work. May be interesting to visit again.
thetaPMT = [i*2*pi/nPMT-2*pi/nPMT/2. for i in range(nPMT)] 	# Angle of PMT with evenly spaced nPMTs
xPMT = x0 + sqrt(R) * np.cos(thetaPMT)
yPMT = y0 + sqrt(R) * np.sin(thetaPMT)
# Graph for PMTs
gPMT = ROOT.TGraph(len(xPMT), xPMT, yPMT)
gPMT.GetXaxis().SetLimits(-R, R)
gPMT.GetYaxis().SetRangeUser(-R, R)
gPMT.SetMarkerStyle(ROOT.kFullDotLarge)
gPMT.SetMarkerColor(ROOT.kYellow-6)
gPMT.SetMarkerSize(1)

### Fiducial volume curve
# From PostAnalyze.C
# if (TMath::ATan(z_mb/r2_mb) < 55.0*TMath::Pi()/180.0  &&  r3_mb < 591.0){...}
Fiducial = ROOT.TArc()
Fiducial.SetLineColor(ROOT.kGray)
Fiducial.SetFillStyle(4000)
FiducialR = 0.6 #591







BulkColor = ROOT.kViolet#+1 #+1
SurfColor = ROOT.kGray+1 #+2
NeckColor = ROOT.kCyan#+1



###### MONTE CARLO OF BULK, SURFACE EVENTS ######

# Generate random events in detector bulk.
nB = 1000							# Number of events
print ' ', nB, 'bulk events.'
# Note that np.random.rand(n) generates points gathered at (0,0), while np.random.uniform(0,R,n) spreads them out ~evenly.
# For simulating a 2D projection of a sphere, np.random.rand() may be more realistic
rB = np.random.uniform(low=0., high=R, size=nB)			# Radius of event
thetaB = np.random.uniform(low=0., high=2.*pi, size=nB)		# Angle of event. Zero theta is far right on this plot.

# Location of points in rectangular coords.
xB = x0 + sqrt(rB)*np.cos(thetaB)
yB = y0 + sqrt(rB)*np.sin(thetaB)
# Save new arrays to delete along with reconstructed events outside detector. Must be lists to del later.
xB2 = list(xB[:])
yB2 = list(yB[:])

# Graph for bulk events
gB = ROOT.TGraph(len(xB), xB, yB)
gB.GetXaxis().SetLimits(-R, R)
gB.GetYaxis().SetRangeUser(-R, R)
gB.SetMarkerStyle(ROOT.kFullDotLarge)
gB.SetMarkerColor(BulkColor)
gB.SetMarkerSize(0.25)


### Generate random events on detector surface
nS = nB
print ' ', nS, 'surface events.'
thetaS = np.random.uniform(low=0., high=2.*pi, size=nS)		# Angle of event, radius = R
xS = x0 + sqrt(R) * np.cos(thetaS)
yS = y0 + sqrt(R) * np.sin(thetaS)
# Save new arrays to delete along with reconstructed events outside detector. Must be lists to del later.
xS2 = list(xS[:])
yS2 = list(yS[:])

# Graph for surface events
gS = ROOT.TGraph(len(xS), xS, yS)
gS.GetXaxis().SetLimits(-R, R)
gS.GetYaxis().SetRangeUser(-R, R)
gS.SetMarkerStyle(ROOT.kFullDotLarge)
gS.SetMarkerColor(SurfColor)
gS.SetMarkerSize(0.25)



### Generate random events on neck
nN = nB
print ' ', nN, 'neck events.'
neckcone = 15*pi/180.
thetaN = np.random.uniform(low=-neckcone, high=neckcone, size=nN)         # Angle of event, radius = R
xN = x0 + sqrt(R) * np.cos(thetaN)
yN = y0 + sqrt(R) * np.sin(thetaN)
# Save new arrays to delete along with reconstructed events outside detector. Must be lists to del later.
xN2 = list(xS[:])
yN2 = list(yS[:])

# Graph for surface events
gN = ROOT.TGraph(len(xN), xN, yN)
gN.GetXaxis().SetLimits(-R, R)
gN.GetYaxis().SetRangeUser(-R, R)
gN.SetMarkerStyle(ROOT.kFullDotLarge)
gN.SetMarkerColor(NeckColor)
gN.SetMarkerSize(0.25)



### Plot bulk, surface, neck events as true Monte Carlo
cMonteCarlo = ROOT.TCanvas('cMonteCarlo', 'cMonteCarlo', 720, 720)
gB.SetTitle('MonteCarlo: nB='+str(nB)+', nS='+str(nS)+', nN='+str(nN)+';;')
gB.Draw('ap')
gS.Draw('same p')
gN.Draw('same p')
Fiducial.DrawArc(0,0,FiducialR,145-90,395-90)
gNeck.Draw('same p')
gPMT.Draw('same p')
#cMonteCarlo.SaveAs('MonteCarlo.png')


















###### "RECONSTRUCTION" OF BULK EVENTS ######

### Wiggle bulk events around by some +- sigma value
sigma = 0.15
xB_R = [x+i for x,i in zip(np.random.uniform(-sigma,sigma,nS), xB)]
yB_R = [y+i for y,i in zip(np.random.uniform(-sigma,sigma,nS), yB)]
#'''#
### Fake centroid reconstruction from PMTs. SINGLE BULK EVENT.
# Highlight single event. Just pick index 1. Why not 0? Because single event is ONE event........
xB_1 = xB[1]
yB_1 = yB[1]
gB_1 = ROOT.TGraph(1, np.array(xB_1), np.array(yB_1))
gB_1.SetMarkerStyle(ROOT.kCircle)
gB_1.SetMarkerColor(ROOT.kViolet+1)
gB_1.SetMarkerSize(1)
gB_1.Draw('same p')

# Draw lines from event to PMTs
line = ROOT.TLine()
line.SetLineColor(ROOT.kGray+1)
line.SetLineStyle(ROOT.kDashed)

xCentroid_1 = []
yCentroid_1 = []
for x,y in zip(xPMT,yPMT):
  xdelta = xB_1-x
  ydelta = yB_1-y
  r = sqrt( (xdelta)**2 + (ydelta)**2 )
  intensity = 1/r**2

  xCentroid_1.append(xdelta)
  yCentroid_1.append(ydelta)
  line.DrawLine(xB_1, yB_1, x, y)

### Plot Reconstructed event position for the single event
xB_RCentroid_1 = np.mean(xCentroid_1)
yB_RCentroid_1 = np.mean(yCentroid_1)

gB_RCentroid_1 = ROOT.TGraph(1, np.array(xB_RCentroid_1), np.array(yB_RCentroid_1))
gB_RCentroid_1.SetMarkerStyle(ROOT.kFullStar)
gB_RCentroid_1.SetMarkerColor(ROOT.kViolet+1)
gB_RCentroid_1.SetMarkerSize(1)
gB_RCentroid_1.Draw('same p')
cMonteCarlo.SaveAs('cCentroid1.png')

### Difference between single true event and single reconstructed event
xDelta_1 = abs(xB_RCentroid_1-xB_1)
yDelta_1 = abs(xB_RCentroid_1-xB_1)
rDelta_1 = sqrt( xDelta_1**2 + yDelta_1**2 )

print ''
print ' DeltaR between MonteCarlo and Reconstructed position of single bulk event: '+str(rDelta_1)+' R'
print ' This is wrong because it skews event towards the neck (lack of PMTs). Number of PMT does not affect DeltaR'
print ''
#'''#

### Fake centroid reconstruction from PMTs. ALL BULK EVENTS.
'''
xB_RCentroid = []
yB_RCentroid = []
for xb,yb in zip(xB_R,yB_R):
  xCentroid = []
  yCentroid = []
  
  for xpmt,ypmt in zip(xPMT,yPMT):
    xdelta = xb-xpmt
    ydelta = yb-ypmt

    xCentroid.append(xdelta)
    yCentroid.append(ydelta)

  xB_RCentroid.append(np.mean(xCentroid))
  yB_RCentroid.append(np.mean(yCentroid))


### del the event if it "reconstructs" outside detector. Into radius notation because an event may still be within 1x1 square.
# Note that via https://stackoverflow.com/questions/13717463/find-the-indices-of-elements-greater-than-x,
# list comprehension may be possible (and better than this 'deleted' loop), but I couldn't get it. See ToyDEAP36002.py.
rB_RCentroid = [sqrt(x**2+y**2) for x,y in zip(xB_RCentroid,yB_RCentroid)]

deleted = 0
for i,r in enumerate(rB_RCentroid):
  if (abs(r) > R):
    del xB_RCentroid[i-deleted], yB_RCentroid[i-deleted]

    deleted = deleted + 1

 
# Graph for bulk events reconstructed
gB_RCentroid = ROOT.TGraph(len(xB_RCentroid), np.array(xB_RCentroid), np.array(yB_RCentroid))
gB_RCentroid.GetXaxis().SetLimits(-R, R)
gB_RCentroid.GetYaxis().SetRangeUser(-R, R)
gB_RCentroid.SetMarkerStyle(ROOT.kFullStar)
gB_RCentroid.SetMarkerColor(ROOT.kViolet+1)
gB_RCentroid.SetMarkerSize(1)


### Difference between single true event and single reconstructed event
xDelta = [abs(xb_r-xb) for xb_r,xb in zip(xB_RCentroid,xB)]
yDelta = [abs(yb_r-yb) for yb_r,yb in zip(yB_RCentroid,yB)]
rDelta = [sqrt( xdelta**2 + ydelta**2 ) for xdelta,ydelta in zip(xDelta,yDelta)]

print ' Mean DeltaR between MonteCarlo and Reconstructed position of bulk events: '+str(np.mean(rDelta))+' R'
print ''
'''

### Don't do that centroid stuff. Is interesting and I've wasted more than 12 hours on playing with it, but just wiggle.
rB_R = [sqrt(x**2+y**2) for x,y in zip(xB_R,yB_R)]
deleted = 0
for i,r in enumerate(rB_R):
  if (abs(r) > R):
    del xB_R[i-deleted], yB_R[i-deleted]
    del xB2[i-deleted], yB2[i-deleted] 		# Also delete this do when comparing MC vs R, event IDs match.
    deleted = deleted + 1

print ' ', len(xB_R), 'bulk events reconstructed within the detector.'

# Graph for bulk events reconstructed
gB_R = ROOT.TGraph(len(xB_R), np.array(xB_R), np.array(yB_R))
gB_R.GetXaxis().SetLimits(-R, R)
gB_R.GetYaxis().SetRangeUser(-R, R)
gB_R.SetMarkerStyle(ROOT.kFullStar)
gB_R.SetMarkerColor(BulkColor)
gB_R.SetMarkerSize(1)





###### "RECONSTRUCTION" OF SURFACE EVENTS ######

### Wiggle surface events around by some +- sigma value
sigma = 0.05
xS_R = [x+i for x,i in zip(np.random.uniform(-sigma,sigma,nS), xS)]
yS_R = [y+i for y,i in zip(np.random.uniform(-sigma,sigma,nS), yS)]

### Then smear wiggled surface events position by Pareto function. Higher alpha means less leakage. Like 1,2,5,10. 
alpha = 5
xS_R = [x/random.paretovariate(alpha) for x in xS]
yS_R = [y/random.paretovariate(alpha) for y in yS]

### del the event if it "reconstructs" outside detector. Into radius notation because an event may still be within 1x1 square.
rS_R = [sqrt(x**2+y**2) for x,y in zip(xS_R,yS_R)]
deleted = 0
for i,r in enumerate(rS_R):
  if (abs(r) > R):
    del xS_R[i-deleted], yS_R[i-deleted]
    del xS2[i-deleted], yS2[i-deleted]            # Also delete this do when comparing MC vs R, event IDs match.
    deleted = deleted + 1
print ' ', len(xS_R), 'surface events reconstructed within the detector.'

# Graph for surface events reconstructed
gS_R = ROOT.TGraph(len(xS_R), np.array(xS_R), np.array(yS_R))
gS_R.SetMarkerStyle(ROOT.kFullStar)
gS_R.SetMarkerColor(SurfColor)
gS_R.SetMarkerSize(1)







###### "RECONSTRUCTION" OF NECK EVENTS ######

### Wiggle neck events around by some +- sigma value
#'''
sigma = 0.05
xN_R = [x+i for x,i in zip(np.random.uniform(-sigma,sigma,nN), xN)]
yN_R = [y+i for y,i in zip(np.random.uniform(-sigma,sigma,nN), yN)]
#'''
### Then smear wiggled neck events position by Pareto function. Higher alpha means less leakage. Like 1,2,5,10. 
alpha = 5
xN_R = [x/random.paretovariate(alpha) for x in xN]
yN_R = [y/random.paretovariate(alpha/10.) for y in yN]

### Wiggle surface events around by some +- sigma value
'''
sigma = 0.05
xN_R = [x+i for x,i in zip(np.random.uniform(-sigma,sigma,nN), xN)]
yN_R = [y+i for y,i in zip(np.random.uniform(-sigma,sigma,nN), yN)]
'''

### del the event if it "reconstructs" outside detector. Into radius notation because an event may still be within 1x1 square.
rN_R = [sqrt(x**2+y**2) for x,y in zip(xN_R,yN_R)]
deleted = 0
for i,r in enumerate(rN_R):
  if (abs(r) > R):
    del xN_R[i-deleted], yN_R[i-deleted]
    del xN2[i-deleted], yN2[i-deleted]            # Also delete this do when comparing MC vs R, event IDs match.
    deleted = deleted + 1
print ' ', len(xN_R), 'neck events reconstructed within the detector.'

# Graph for surface events reconstructed
gN_R = ROOT.TGraph(len(xN_R), np.array(xN_R), np.array(yN_R))
gN_R.SetMarkerStyle(ROOT.kFullStar)
gN_R.SetMarkerColor(NeckColor)
gN_R.SetMarkerSize(1)







### Plot events as Reconstructed
cReconstruction = ROOT.TCanvas('cReconstruction', 'cReconstruction', 720, 720)
#gB_RCentroid.SetTitle('Reconstruction: nB='+str(nB)+', nS='+str(nS)+';;')
#gB_RCentroid.Draw('ap')
gB_R.SetTitle('Reconstruction: nB='+str(len(xB_R))+', nS='+str(len(xS_R))+', nN='+str(len(xN_R))+';;')
gB_R.Draw('ap')
gS_R.Draw('p same')
gN_R.Draw('p same')
Fiducial.DrawArc(0,0,FiducialR,145-90,395-90)
gNeck.Draw('same p')
gPMT.Draw('same p')
#cReconstruction.SaveAs('Reconstruction.png')





### Save all events that reconstructed inside detector
'''
with open('MonteCarlo_Bulk.txt', 'w') as file:
  for x,y in zip(xB2,yB2):
    file.write(str(x)+',')
    file.write(str(y)+'\n')

with open('MonteCarlo_Surf.txt', 'w') as file:
  for x,y in zip(xS2,yS2):
    file.write(str(x)+',')
    file.write(str(y)+'\n')

with open('Reconstruction_Bulk.txt', 'w') as file:
  for x,y in zip(xB_R,yB_R):
    file.write(str(x)+',')
    file.write(str(y)+'\n')

with open('Reconstruction_Surf.txt', 'w') as file:
  for x,y in zip(xS_R,yS_R):
    file.write(str(x)+',')
    file.write(str(y)+'\n')
'''


'''
with open('ToyDEAP_'+str(len(xB_R+xS_R+xN_R))+'-'+str(nB+nS+nN)+'.data', 'w') as file:
  for xb,yb in zip(xB_R,yB_R):
    file.write(str(xb)+',')
    file.write(str(yb)+',')
    file.write('bulk\n')
  for xs,ys in zip(xS_R,yS_R):
    file.write(str(xs)+',')
    file.write(str(ys)+',')
    file.write('surf\n')
  for xn,yn in zip(xN_R,yN_R):
    file.write(str(xn)+',')
    file.write(str(yn)+',')
    file.write('neck\n')
'''
