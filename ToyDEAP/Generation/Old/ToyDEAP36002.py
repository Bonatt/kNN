import ROOT
import numpy as np
import math as math
import os
import linecache
import sys
import random


# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return np.sqrt(x)
# Redefine pi to something shorter
pi = np.pi    

#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();
print ''




### Inspired by DEAP-3600 surface background problem.
# I've done this before via GEANT4 Monte Carlo (and some real data), but let's try real kNN with a toy model:
# 2D only.
# I. For surface background events:
#   i. Generate events (uniformly) on a circle (MC true surface).
#   ii. Randomly move them around, preferably preferentially towards the center (Reconstruction) 
# II. For bulk events:
#   i. Generate events (uniformly) on disk (MC true bulk).
#   ii. Optional. Randomly move them around. This doesn't really effect much, but is akin to more reconstruction analysis.
# III. With this training data, use kNN to parse these points into surface/bulk.
# IV. Generate new data for it try.

# Much of this code was pulled from AvgDistInCircle




### DEAP-3600 parameters. May not be used much.
DEAPRadius = 0.85                                                       # m
DEAPSurfaceArea = 4.*pi*DEAPRadius**2                                # m2, roughly outside
DEAPVolume = 4./3.*pi*DEAPRadius**3                                  # m3
DEAPCircumference = 2.*pi*DEAPRadius





# Define unit circle parameters centered at (0,0).
R = 1.								# Radius of detector
x0 = 0.
y0 = 0.




###### Create various interesting aspects regarding detector ######

### Draw center of detector
'''
g0 = ROOT.TGraph(1, np.array(x0), np.array(y0))
g0.SetMarkerStyle(ROOT.kFullDotLarge)
g0.SetMarkerSize(0.25)
'''
### Draw neck of detector
gNeck = ROOT.TGraph(1, np.array(R), np.array(y0))
gNeck.SetMarkerStyle(ROOT.kCircle)
gNeck.SetMarkerColor(ROOT.kGray+2)
gNeck.SetMarkerSize(1)

### Generate "PMT" locations on detector surface
nPMT = 19                                                       # Counted lightguides in standard cut drawing instead.
neckoffset = 30.*pi/180.					# Degrees*pi/180 = rad
print ' ', nPMT, 'PMTs.'
thetaPMT = [i*((2*pi-neckoffset)/nPMT)+neckoffset for i in range(nPMT)] # angle of PMT. From neckoffset, evenly spaced nPMTs
xPMT = x0 + sqrt(R) * np.cos(thetaPMT)
yPMT = y0 + sqrt(R) * np.sin(thetaPMT)
# Graph for PMTs
gPMT = ROOT.TGraph(len(xPMT), xPMT, yPMT)
gPMT.GetXaxis().SetLimits(-R, R)
gPMT.GetYaxis().SetRangeUser(-R, R)
gPMT.SetMarkerStyle(ROOT.kFullDotLarge)
gPMT.SetMarkerColor(ROOT.kYellow-6)
gPMT.SetMarkerSize(1)

### Fiducial volume curve
# From PostAnalyze.C
# if (TMath::ATan(z_mb/r2_mb) < 55.0*TMath::Pi()/180.0  &&  r3_mb < 591.0){...}
Fiducial = ROOT.TArc()
Fiducial.SetLineColor(ROOT.kGray)
Fiducial.SetFillStyle(4000)
FiducialR = 0.6 #591








###### MONTE CARLO OF BULK, SURFACE EVENTS ######

# Generate random events in detector bulk.
nB = 1000							# Number of events
print ' ', nB, 'bulk events.'
# Note that np.random.rand(n) generates points gathered at (0,0), while np.random.uniform(0,R,n) spreads them out ~evenly.
# For simulating a 2D projection of a sphere, np.random.rand() may be more realistic
rB = np.random.uniform(low=0., high=R, size=nB)			# Radius of event
thetaB = np.random.uniform(low=0., high=2.*pi, size=nB)		# Angle of event. Zero theta is far right on this plot.

# Location of points in rectangular coords.
xB = x0 + sqrt(rB)*np.cos(thetaB)
yB = y0 + sqrt(rB)*np.sin(thetaB)

# Graph for bulk events
gB = ROOT.TGraph(len(xB), xB, yB)
gB.GetXaxis().SetLimits(-R, R)
gB.GetYaxis().SetRangeUser(-R, R)
gB.SetMarkerStyle(ROOT.kFullDotLarge)
gB.SetMarkerColor(ROOT.kViolet+1)
gB.SetMarkerSize(0.25)


### Generate random events on detector surface
nS = nB
print ' ', nS, 'surface events.'
thetaS = np.random.uniform(low=0., high=2.*pi, size=nS)		# Angle of event, radius = R
xS = x0 + sqrt(R) * np.cos(thetaS)
yS = y0 + sqrt(R) * np.sin(thetaS)

# Graph for surface events
gS = ROOT.TGraph(len(xS), xS, yS)
gS.GetXaxis().SetLimits(-R, R)
gS.GetYaxis().SetRangeUser(-R, R)
gS.SetMarkerStyle(ROOT.kFullDotLarge)
gS.SetMarkerColor(ROOT.kGray+2)
gS.SetMarkerSize(0.25)


### Plot bulk, surface events as true Monte Carlo
cMonteCarlo = ROOT.TCanvas('cMonteCarlo', 'cMonteCarlo', 720, 720)
gB.SetTitle('MonteCarlo: nB='+str(nB)+', nS='+str(nS)+';;')
gB.Draw('ap')
gS.Draw('same p')
Fiducial.DrawArc(0,0,FiducialR,145-90,395-90)
gNeck.Draw('same p')
gPMT.Draw('same p')
cMonteCarlo.SaveAs('MonteCarlo.png')










###### "RECONSTRUCTION" OF BULK EVENTS ######

### Fake centroid reconstruction from PMTs. SINGLE BULK EVENT.
# Highlight single event. Just pick index 1. Why not 0? Because single event is ONE event........
xB_1 = xB[1]
yB_1 = yB[1]
gB_1 = ROOT.TGraph(1, np.array(xB_1), np.array(yB_1))
gB_1.SetMarkerStyle(ROOT.kCircle)
gB_1.SetMarkerColor(ROOT.kViolet+1)
gB_1.SetMarkerSize(1)
gB_1.Draw('same p')

# Draw lines from event to PMTs
line = ROOT.TLine()
line.SetLineColor(ROOT.kGray+1)
line.SetLineStyle(ROOT.kDashed)

xCentroid_1 = []
yCentroid_1 = []
for x,y in zip(xPMT,yPMT):
  
  xdelta = xB_1-x
  ydelta = yB_1-y
  r = sqrt( (xdelta)**2 + (ydelta)**2 )
  intensity = 1/r**2

  xCentroid_1.append(xdelta)
  yCentroid_1.append(ydelta)
  line.DrawLine(xB_1, yB_1, x, y)

### Plot Reconstructed event position for the single event
xB_R_1 = np.mean(xCentroid_1)
yB_R_1 = np.mean(yCentroid_1)

gB_R_1 = ROOT.TGraph(1, np.array(xB_R_1), np.array(yB_R_1))
gB_R_1.SetMarkerStyle(ROOT.kFullStar)
gB_R_1.SetMarkerColor(ROOT.kViolet+1)
gB_R_1.SetMarkerSize(1)
gB_R_1.Draw('same p')
cMonteCarlo.SaveAs('MonteCarlo2.png')

### Difference between single true event and single reconstructed event
xDelta_1 = abs(xB_R_1-xB_1)
yDelta_1 = abs(xB_R_1-xB_1)
rDelta_1 = sqrt( xDelta_1**2 + yDelta_1**2 )

print ''
print ' DeltaR between MonteCarlo and Reconstructed position of single bulk event: '+str(rDelta_1)+' R'
print ' This is wrong because it skews event towards the neck (lack of PMTs). Number of PMT does not affect DeltaR'
print ''




### Fake centroid reconstruction from PMTs. ALL BULK EVENTS.
xB_R = []
yB_R = []
for xb,yb in zip(xB,yB):

  xCentroid = []
  yCentroid = []
  for xpmt,ypmt in zip(xPMT,yPMT):

    xdelta = xb-xpmt
    ydelta = yb-ypmt

    xCentroid.append(xdelta)
    yCentroid.append(ydelta)

  xB_R.append(np.mean(xCentroid))
  yB_R.append(np.mean(yCentroid))
 
# Graph for bulk events reconstructed
gB_R = ROOT.TGraph(len(xB_R), np.array(xB_R), np.array(yB_R))
gB_R.GetXaxis().SetLimits(-R, R)
gB_R.GetYaxis().SetRangeUser(-R, R)
gB_R.SetMarkerStyle(ROOT.kFullStar)
gB_R.SetMarkerColor(ROOT.kViolet+1)
gB_R.SetMarkerSize(1)


### Difference between single true event and single reconstructed event
xDelta = [abs(xb_r-xb) for xb_r,xb in zip(xB_R,xB)]
yDelta = [abs(yb_r-yb) for yb_r,yb in zip(yB_R,yB)]
rDelta = [sqrt( xdelta**2 + ydelta**2 ) for xdelta,ydelta in zip(xDelta,yDelta)]

print ' Mean DeltaR between MonteCarlo and Reconstructed position of bulk events: '+str(np.mean(rDelta))+' R'
print ''










###### "RECONSTRUCTION" OF SURFACE EVENTS ######

### Wiggle surface events around by some +- sigma value
'''
sigma = 0.05
xS_R = [x+i for x,i in zip(np.random.uniform(-sigma,sigma,nS), xS)]
yS_R = [y+i for y,i in zip(np.random.uniform(-sigma,sigma,nS), yS)]
'''

### Or determine some kind of wiggle function. Maybe expo modified gaussian, smearing more events inwards than outwards.
rangemin = -1.
rangemax = 1.

# gaus. A gaussian with 3 parameters: f(x) = p0*exp(-0.5*((x-p1)/p2)^2)).
fGaus = ROOT.TF1('fGaus', 'gaus',rangemin,rangemax)
fGaus.SetLineColor(ROOT.kBlue)
fGaus.SetParameter(0,1) # amp
fGaus.SetParameter(1,0) # center
fGaus.SetParameter(2,1) # sigma

# expo. An exponential with 2 parameters: f(x) = exp(p0+p1*x).
fExpo = ROOT.TF1('fExpo', 'expo/[2]',rangemin,rangemax)
fExpo.SetLineColor(ROOT.kRed)
fExpo.SetParameter(0,0) # const
fExpo.SetParameter(1,1) # that main factor
fExpo.SetParameter(2,1)#7.38905609893065) # fExpo(1) without /[2]. Normalizing to 1.

F = ROOT.TF1('F', '(fGaus+fExpo)/[6]', rangemin,rangemax)
F.SetTitle(';;')
#F = ROOT.TF1('F', '(fGaus*fExpo)/[6]', 0,1)
F.SetParameter(6,1)#1.6065306597126334) # F(1) (+ version) without /[6]. Normalizing to 1.
F.SetLineColor(ROOT.kViolet)

cF = ROOT.TCanvas('cF', 'cF', 720, 720)
F.Draw()
fGaus.Draw('same')
fExpo.Draw('same')

# Shift surface events position by some function. Always closer to (0,0) because F>1.
# I think np.random.rand() is similar to fGaus, in that they both tend to produce a number preferentially near center of range.
# If rangeF = 1 (R), then there seems to be a hard line near the "mantle" of the detector. Increasing the bounds gives more range,
# but then surface events fill the whole detector... 
# Let max range be surface, so it only gets closer to center. The larger the minrange, the more "uniform" into the detector leakage is.
# Normalize random number my max value so we don't get numbers generated like 7. Want only 0<f<1.

xS_R = [x*abs(random.paretovariate(1)-100)/99. for x in xS]
yS_R = [y*abs(random.paretovariate(1)-100)/99. for y in yS]

# Replace all conditions met with 1.0, i.e. remove the few (much greater than) (-)1's that come up.
'''
gg = 1.0
for i, (x,y) in enumerate(zip(xS_R, yS_R)):
  if (x > gg): xS_R[i] = gg
  if (y > gg): yS_R[i] = gg
  if (x < -gg): xS_R[i] = -gg
  if (y < -gg): yS_R[i] = -gg
'''


### Just del the event if it "reconstructs" outside detector

boundary = 1.0
'''
deleted = 0
for i, (x,y) in enumerate(zip(xS_R, yS_R)):
  print i,x,y
  if (x > boundary) or (x < -boundary) or (y > boundary) or (y < -boundary):
    print i, '###############################################################'
    del xS_R[i]
    del yS_R[i]

  if (x > boundary) == True: del xS_R[i], yS_R[i]; print '1 ###########'
  if (x < -boundary) == True: del xS_R[i], yS_R[i]; print '2 ###########'
  if (y > boundary) == True: del xS_R[i], yS_R[i]; print '3 ###########'
  if (y < -boundary) == True: del xS_R[i], yS_R[i]; print '4 ###########'
  if (x > boundary) or (x < -boundary) or (y > boundary) or (y < -boundary):
    del xS_R[i-deleted]
    del yS_R[i-deleted]
    deleted = deleted + 1    
'''

# encore
'''
deleted = 0
for i, (x,y) in enumerate(zip(xS_R, yS_R)):
  if (x > boundary) or (x < -boundary) or (y > boundary) or (y < -boundary):
    del xS_R[i-deleted]
    del yS_R[i-deleted]
    deleted = deleted + 1 
'''


### Flag incides out of bounds. Delete those
'''
xS_R_oob = [x for x in xS_R if (x > boundary) or (x < -boundary)]


[zip(x,y) for (x,y) in zip(xS_R,yS_R) if (x > boundary) or (x < -boundary) or (y > boundary) or (y < -boundary)]
'''


### List comprehension instead
#[x for (x,y) in zip(xS_R,yS_R) if (x > boundary) or (x < -boundary) or (y > boundary) or (y < -boundary)]
#len([x for (x,y) in zip(xS_R,yS_R) if ((x < boundary) and (abs(x) < boundary)) or ((y < boundary) and (abs(y) > boundary))])
#len([x for (x,y) in zip(xS_R,yS_R) if (x**2 > boundary) or (y**2 > boundary)])
#len([x for (x,y) in zip(xS_R,yS_R) if (abs(x) > boundary) or (abs(y) > boundary)])
#[(x,y) for (x,y) in zip(xS_R,yS_R) if (abs(x) > boundary) or (abs(y) > boundary)]
# len([y for (x,y) in zip(xS_R,yS_R) if (abs(x) < boundary) and (abs(y) < boundary)])

#xS_R = [x for x,y in zip(xS_R,yS_R) if (abs(x) < boundary) and (abs(y) < boundary)]
#yS_R = [y for x,y in zip(xS_R,yS_R) if (abs(x) < boundary) and (abs(y) < boundary)]


############### https://stackoverflow.com/questions/13717463/find-the-indices-of-elements-greater-than-x



### Okay, deleting doesn't seem to be working... so append a new list when inside boundary.
'''
xS_Rn = []
yS_Rn = []
for x,y in zip(xS_R, yS_R):
  if (x < boundary) or (x > -boundary) or (y < boundary) or (y > -boundary):
    xS_Rn.append(x)
    yS_Rn.append(y)

xS_R = xS_Rn[:]
yS_R = yS_Rn[:]
'''


### Radius notation.
rS_R = [sqrt(x**2+y**2) for x,y in zip(xS_R,yS_R)]
deleted = 0
for i,r in enumerate(rS_R):
  #if (r > boundary) or (r < -boundary):
  if (abs(r) > boundary):
    del xS_R[i-deleted], yS_R[i-deleted]
    deleted = deleted + 1


# [i for i in j if i < 5]


#xS_R = [x*F(np.random.uniform(rangemin,rangemax))/F(rangemax) for x in xS]
#yS_R = [y*F(np.random.uniform(rangemin,rangemax))/F(rangemax) for y in yS]
#xS_R = [x*fGaus(np.random.uniform(rangemin,rangemax)) for x in xS]
#yS_R = [y*fGaus(np.random.uniform(rangemin,rangemax)) for y in yS]
#xS_R = [x*fGaus(np.random.rand()) for x in xS]
#yS_R = [y*fGaus(np.random.rand()) for y in yS]
#xS_R = [x*fExpo(np.random.rand())/fExpo(rangemax) for x in xS]
#yS_R = [y*fExpo(np.random.rand())/fExpo(rangemax) for y in yS]

#xS_R = [x*random.gauss(0,1) for x in xS]
#yS_R = [y*random.gauss(0,1) for y in yS]
#xS_R = [x*random.gauss(0,1) for x in xS]
#yS_R = [y*random.gauss(0,1) for y in yS]
#xS_R = [x*random.expovariate(1) for x in xS]
#yS_R = [y*random.expovariate(1) for y in yS]
#xS_R = [x*fGaus(random.gauss(0,1)) for x in xS]
#yS_R = [y*fGaus(random.gauss(0,1)) for y in yS]

#map(fExpo,xS[:10])




# Graph for bulk events reconstructed
gS_R = ROOT.TGraph(len(xS_R), np.array(xS_R), np.array(yS_R))
gS_R.SetMarkerStyle(ROOT.kFullStar)
gS_R.SetMarkerColor(ROOT.kGray+2)
gS_R.SetMarkerSize(1)




### Plot events as Reconstructed
cReconstruction = ROOT.TCanvas('cReconstruction', 'cReconstruction', 720, 720)
gB_R.SetTitle('Reconstructionstructed: nB='+str(nB)+', nS='+str(nS)+';;')
#gB_R.Draw('ap')
gS_R.Draw('ap')
Fiducial.DrawArc(0,0,FiducialR,145-90,395-90)
gNeck.Draw('same p')
gPMT.Draw('same p')
cReconstruction.SaveAs('Reconstruction.png')







#xB_r = [r*i for r,i in zip(np.random.uniform(0.,2.*pi,nB), xB)]
