import numpy as np
import random
import math
from operator import itemgetter
import time
import csv
from collections import Counter


# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return math.sqrt(x)
# Redefine pi to something shorter
pi = math.pi


### From http://machinelearningmastery.com/tutorial-to-implement-k-nearest-neighbors-in-python-from-scratch/
# Edited to take ToyDEAP3600 data, which I put to file in same manner as iris.data. 
# Rerewriting this code because my first attempt to emulate this code "in my own words" results in worse timing: his ~6 s vs my ~42 s.
# See "FollowingJason" for that attempt.


# Takes data in the form [0.0763758342428, -0.377705472767, bulk] where nFeatures is 2 here. Third is event ID.
#File = 'ToyDEAP_1923-2000.data'; nFeatures = 2
#File = 'ToyDEAP_3855-4000.data'; nFeatures = 2
#File = 'ToyDEAP_4830-5000.data'; nFeatures = 2
#File = 'ToyDEAP_7710-8000.data'; nFeatures = 2
#File = 'ToyDEAP_9636-10000.data'; nFeatures = 2
File = 'iris.data'; nFeatures = 4
Delimiter = ','

### Use some percent of data if desired, e.g. 100/100. == 100% of data.
if File == 'iris.data':
  PercentOfData = 100/100.
else:
  PercentOfData = 100/100.



### Load data, splitting data randomly into training or test sets. 
def LoadData(filename, split, trainingset=[] , testset=[]):
  # filename = 'ToyDEAP.data'
  # splti = 2/3.
  # trainingset, testset = ...
  #'''
  with open(filename, 'rb') as csvfile:
      lines = csv.reader(csvfile)
      dataset = list(lines)
      for x in xrange(len(dataset)-1):
          for y in xrange(nFeatures):
              dataset[x][y] = float(dataset[x][y])
          if random.random() < split:
              trainingset.append(dataset[x])
          else:
              testset.append(dataset[x])
  #'''
  '''
  with open(filename, 'rb') as csvfile:
    dataset = list(csv.reader(csvfile))
    for row in dataset:
      # Make features from string to float
      for f in xrange(nFeatures):
        row[f] = float(row[f])
      if random.random() < split:
        trainingset.append(row)
      else:
        testset.append(row)
  '''
  # Mine: 5 s above, 25 s below. 25 s without PercentOfData. Not sure why, when timing just LoadData, 0.12 s above, 0.45 s below.
  '''
  data = np.genfromtxt(filename, delimiter=Delimiter, dtype=None)
  print 'Full dataset:', len(data)

  data = data[:int(len(data)*PercentOfData)]
  print 'Analyzed dataset:', len(data)

  for instance in data:
    if random.random() < split:
      trainingset.append(instance)
    else:
      testset.append(instance)
  '''
  return len(trainingset), len(testset)


### Define Euclidean distance This distance-calculating method may vary (Manhattan, Hamming, ...), so define explicitly as such.
# r = sqrt( (x-x0)**2 + ... ) for 1D. Add (y-y0)**2 for 2D, etc.
''' 
def EuclideanDistance(instance1, instance2, length):
  distance = 0
  for x in xrange(length):
    distance += pow((instance1[x] - instance2[x]), 2)
  return math.sqrt(distance)
'''
'''
def EuclideanDistance(instance1, instance2):
  distance = 0
  for x in xrange(nFeatures):
    distance += pow((instance1[x] - instance2[x]), 2)
  return sqrt(distance)
'''
'''
def EuclideanDistance(instance1, instance2):
  distance = 0
  for f1,f2 in zip(instance1[:-1],instance2[:-1]):
    distance += (f1 - f2)**2
  return sqrt(distance)
'''
def EuclideanDistance(instance1, instance2):
  distance = 0
  for f in xrange(nFeatures):
    distance += (instance1[f] - instance2[f])**2
  return distance#sqrt(distance)


 
### Determine k neighbors. Take in full training dataset and single test instance. 
# Compute distances between single test instance and every training instance (this is looped from main() for every test instance).
# Take k smallest distances.
def GetNeighbors(trainingset, lentrainingset, testinstance, k):
  '''
  distances = []
  length = len(testinstance)-1
  for x in xrange(len(trainingset)):
    dist = EuclideanDistance(testinstance, trainingset[x])
    distances.append((trainingset[x], dist))
  distances.sort(key=itemgetter(1))
  neighbors = []
  for x in xrange(k):
    neighbors.append(distances[x][0])
  return neighbors
  '''
  '''
  distances = []
  for traininginstance in trainingset:
    distance = EuclideanDistance(traininginstance, testinstance)
    distances.append( (traininginstance, distance) )
  # distances has form [((-0.354481106014, -0.784429584872, 'bulk'), 0.611469612650144), ...] = [((x,y,id),r), ...]
  # Sort by r. Take only first k neighbors. Then take only k neighbors without attached distance.
  distances.sort(key=itemgetter(-1))
  neighbors = [i[0] for i in distances[:k]]
  return neighbors
  '''
  '''
  distances = []
  for x in xrange(lentrainingset):
    dist = EuclideanDistance(testinstance, trainingset[x])
    distances.append((trainingset[x], dist))
  distances.sort(key=itemgetter(1))
  neighbors = []
  for x in xrange(k):
    neighbors.append(distances[x][0])
  return neighbors, len(neighbors)
  '''
  distances = []
  for traininginstance in trainingset:
    distance = EuclideanDistance(traininginstance, testinstance)
    distances.append((traininginstance, distance))
  distances.sort(key=itemgetter(1))
  #'''
  neighbors = []
  for d in distances[:k]:
    neighbors.append(d[0])
  return neighbors 
  #'''
  # This is slow because I create new list from full distances list
  '''
  neighbors = [i[:k][0] for i in distances]
  return neighbors
  '''
  # Better because only use kth of distances list. Still slower than og.
  #return [i[0] for i in distances[:k]]


### Get response, i.e. vote. Assume classification is index [-1] of neighbors. Counts how many times each classification appears. 
def GetResponse(neighbors):
  '''
  classVotes = {}
  #for x in xrange(lenneighbors):
  for x in xrange(k)
    response = neighbors[x][-1]
    if response in classVotes:
      classVotes[response] += 1
    else:
      classVotes[response] = 1
  sortedVotes = sorted(classVotes.iteritems(), key=itemgetter(1), reverse=True)
  return sortedVotes[0][0]
  '''
  responses = [i[-1] for i in neighbors]
  #response = Counter(responses).most_common(1)[0][0]
  #return response
  return Counter(responses).most_common(1)[0][0]



### How accurate was response? This could be done in above loop... 
def GetAccuracy(testset, lentestset, predictions):
  '''
  correct = 0
  for x in xrange(lentestset):
    if testset[x][-1] == predictions[x]:
      correct += 1
  return (correct/float(lentestset)) * 100.0
  '''
  correct = 0
  for t,p in zip(testset,predictions):
    if t[-1] == p:
      correct += 1
  return (correct/float(lentestset)) * 100.0

 


 
def main():
  start = time.clock()

  ### 67% of data to train model, last 33% to test model. 
  Split = 2/3.

  ### k-nearest neighbors. How many nearest neighbors?
  k = 3

  # prepare data
  trainingset=[]
  testset=[]
  lentrainingset, lentestset = LoadData(File, Split, trainingset, testset)
  print 'Train set: ' + repr(lentrainingset)
  print 'Test set: ' + repr(lentestset)

  # generate predictions
  '''
  predictions=[]  
  for x in xrange(lentestset):
    neighbors, lenneighbors = GetNeighbors(trainingset, lentrainingset, testset[x], k)
    result = GetResponse(neighbors, lenneighbors)
    predictions.append(result)
    #print x,'of', lentestset, ('> predicted=' + repr(result) + ', actual=' + repr(testset[x][-1]))
  '''
  predictions=[]  
  for testinstance in testset:
    neighbors = GetNeighbors(trainingset, lentrainingset, testinstance, k)
    result = GetResponse(neighbors)
    predictions.append(result)

  accuracy = GetAccuracy(testset, lentestset, predictions)
  print('Accuracy: ' + repr(accuracy) + '%')
  end = time.clock()
  print 'Time:', end-start, 's'





### Actually run program. I modified it to loop over values of k (, split).
Loops = 3
print ''
for m in xrange(Loops):
  main()
  print ''


#import profile  
#profile.run('main()')


#main()



'''
### 67% of data to train model, last 33% to test model. 
Split = 2/3.
### k-nearest neighbors. How many nearest neighbors?
k = 3
trainingset=[]
testset=[]
start = time.clock()
lentrainingset, lentestset = LoadData(File, Split, trainingset, testset)
end = time.clock()
print 'Time:', end-start, 's'
'''



