import numpy as np
import random
import time
from collections import Counter
#import operator
from operator import itemgetter
import math
import sys



# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return math.sqrt(x)
# Redefine pi to something shorter
pi = math.pi


### From http://machinelearningmastery.com/tutorial-to-implement-k-nearest-neighbors-in-python-from-scratch/
# Edited to take ToyDEAP3600 data, which I put to file in same manner as iris.data.
# Takes data in the form [0.0763758342428, -0.377705472767, bulk] where nDatacolumns is 2 here. Third is event ID.





### Original's is still waaaay faster. Why? Stop using mine.
'''
[14:04:53]~/Other/FunCode/MachineLearning/kNN/ToyDEAP/Classification$ py FromJason.py 
Train set: 1242
Test set: 627
Accuracy: 64.91228070175438%
Time: 5.91 s

[14:04:59]~/Other/FunCode/MachineLearning/kNN/ToyDEAP/Classification$ py FollowingJason3.py 
Data: ToyDEAP.data
Percent of data analyzed: 100%
Split: 0.67
k: 2
Full dataset: 1870
Analyzed dataset: 1870
Training set: 1267
Test set: 603
Accuracy: 61.86%
Time: 42.19 s
'''










### Define data file, number of features, and data delimiter. Assumes n cols of feature doubles with n+1 col as classification string.
#Filename = 'iris.data'
Filename = 'ToyDEAP.data'
Delimiter = ','

### Use some percent of data if desired, e.g. 100/100. == 100% of data.
if Filename == 'iris.data':
  PercentOfData = 100/100.
else:
  PercentOfData = 100/100.

### 67% of data to train model, last 33% to test model. 
Split = 2/3.

### k-nearest neighbors. How many nearest neighbors?
k = 3




### Load data, splitting data randomly into training or test sets.
def LoadData(filename, split, trainingset=[], testset=[]):
  data = np.genfromtxt(filename, delimiter=Delimiter, dtype=None)
  print 'Full dataset:', len(data)
  
  data = data[:int(len(data)*PercentOfData)]
  print 'Analyzed dataset:', len(data)

  # If data is not shuffled, do this:
  #'''
  for instance in data:
    if random.random() < split:
      trainingset.append(instance)
    else:
      testset.append(instance)
  #'''
  # Otherwise, just do this. Even shuffle to randomize too! FYI this is wrong for iris.data, somehow. Produces 70% accuracy, not 95%.
  '''
  random.shuffle(data)
  trainingset.extend(data[:int(len(data)*split)])
  testset.extend(data[int(len(data)*split):])
  '''
  # Or... From https://stackoverflow.com/questions/6482889/get-random-sample-from-list-while-maintaining-ordering-of-items
  #[ a[i] for i in sorted(random.sample(xrange(len(a)), len(a))) ]
  #[ a[i] for i in random.sample(xrange(len(a)), len(a)) ]

  print 'Training set:', len(trainingset)
  print 'Test set:', len(testset)
 

### Define Euclidean distance This distance-calculating method may vary (Manhattan, Hamming, ...), so define explicitly as such.
# r = sqrt( (x-x0)**2 + ... ) for 1D. Add (y-y0)**2 for 2D, etc.
def EuclideanDistance(instance1, instance2):
  #print 'i1='+str(instance1)+';i2='+str(instance2)
  features1 = list(instance1)[:-1]
  features2 = list(instance2)[:-1]
  #print 'f1='+str(features1)+' \nf2='+str(features2)
  #r = sqrt(sum([(f1-f2)**2 for f1,f2 in zip(features1, features2)]))
  r = sum([(f1-f2)**2 for f1,f2 in zip(features1, features2)])
  return r

### Define another distance.
def Distance(i1, i2):
  f1 = list(i1)[:-1]
  f2 = list(i2)[:-1]
  #return sum(list(set(i1[:-1])-set(i2[:-1])))
  return sum(list(set(f1)-set(f2)))


#def Distance(trainingdata, testinstance):
#  distances = sqrt(((trainingdata-testinstance)**2).sum(axis=1))
#  return distances








### Determine k neighbors. Take in full training dataset and single test instance. 
# Compute distances between single test instance and every training instance (this is looped from main() for every test instance).
# Take k smallest distances.
def GetNeighbors(trainingset, testinstance, k):
  '''
  distances = []
  for traininginstance in trainingset:
    distance = EuclideanDistance(traininginstance, testinstance)
    distances.append( (traininginstance, distance) )
  # distances has form [((-0.354481106014, -0.784429584872, 'bulk'), 0.611469612650144), ...] = [((x,y,id),r), ...]
  # Sort by r. Take only first k neighbors. Then take only k neighbors without attached distance.
  neighbors_distances = sorted(distances, key=lambda r: r[-1])[:k]
  neighbors = [i[0] for i in neighbors_distances]
  return neighbors
  '''
  # 13 s above vs 18 s below for 5% (929) data.
  '''
  distances = [EuclideanDistance(traininginstance, testinstance) for traininginstance in trainingset]
  neighbors_distances = [(i,d) for i,d in zip(trainingset,distances)]
  neighbors_distances = SortBy(neighbors_distances,-1)
  neighbors = [i[0] for i in neighbors_distances[:k]]
  return neighbors
  '''
  # Slowest and least accurate??
  '''
  distances = [EuclideanDistance(traininginstance, testinstance) for traininginstance in trainingset]
  sortedkneighbors = [x for (x,y) in sorted(zip(trainingset,distances))][:k]
  return sortedkneighbors
  '''
  # Redux of forst method. itemgetter instead of lambda
  #'''
  distances = []
  for traininginstance in trainingset:
    #print 'traininginstance =', traininginstance, '; testinstance =', testinstance
    distance = EuclideanDistance(traininginstance, testinstance)
    #distance = Distance(traininginstance, testinstance)
    distances.append( (traininginstance, distance) )
    #print distances
  # distances has form [((-0.354481106014, -0.784429584872, 'bulk'), 0.611469612650144), ...] = [((x,y,id),r), ...]
  # Sort by r. Take only first k neighbors. Then take only k neighbors without attached distance.
  #print distances
  distances.sort(key=itemgetter(-1))
  neighbors = [i[0] for i in distances[:k]]
  return neighbors
  #'''
  # From http://haltingproblems.blogspot.ca/2008/04/k-nearest-neighbors-speed-up.html
  '''
  # get array of distances between item and all others
  distances = sqrt(((trainingset-testinstance)**2).sum(axis=1))
  #get closest item to start with
  closest = distances.argmin()
  neighbors = (closest, )
  #set current closest to large number and find next
  for i in range(1, k):
    distances[closest] = sys.maxint
    closest = distances.argmin()
    neighbors += (closest, ) 
  return neighbors
  '''
'''
data = np.genfromtxt(Filename, delimiter=Delimiter, dtype=None)
trainingset = []; testset = []
for i in data:
  if random.random() < Split: trainingset.append(i)
  else: testset.append(i)

testinstance = testset[0]
'''


### Get response, i.e. vote. Assume classification is index [-1] of neighbors. Counts how many times each classification appears.
def GetResponse(neighbors):
  responses = [i[-1] for i in neighbors]
  response = Counter(responses).most_common(1)[0][0]
  return response


### How accurate was response? This could be done in above loop...
def GetAccuracy(testset, predictions):
  correct = 0
  for id,p in zip(testset, predictions):
    if id[-1] == p:
      correct = correct + 1
  accuracy = correct/float(len(testset))*100.
  return accuracy



### Sort functions:
# From https://wiki.python.org/moin/PythonSpeed/PerformanceTips
def SortBy(somelist, n):
    nlist = [(x[n], x) for x in somelist]
    nlist.sort()
    return [val for (key, val) in nlist]
'''
a = xrange(100); random.shuffle(a)
b = xrange(100); random.shuffle(b)
c = [str(i) for i in xrange(100)]
d = xrange(100); random.shuffle(d)
#e = [((i,j,k),l) for i,j,k,l in zip(a,b,c,d)]
e = [(i,j,k) for i,j,k in zip(a,b,c)]
#f = [x for (x,y) in sorted(zip(e,d))]
#f[:4]
[x for (x,y) in sorted(zip(e,d))][:4]

SortBy(e,-1)
'''
# From https://stackoverflow.com/questions/3855537/fastest-way-to-sort-in-python
def QuickSort(inlist):
    if inlist == []: 
        return []
    else:
        pivot = inlist[0]
        lesser = QuickSort([x for x in inlist[1:] if x < pivot])
        greater = QuickSort([x for x in inlist[1:] if x >= pivot])
        return lesser + [pivot] + greater




### I think the primary benefit of putting all "running" code under a main() function is so one can import without "running".
# However, these variables are not saved outside, and cannot be called later for troubleshotting.
def main(k,split):
  start = time.clock()

  print ''
  print 'Data:', Filename
  print 'Percent of data analyzed:', str(int(PercentOfData*100))+'%'
  print 'Split:', round(split,2)
  print 'k:', k

  TrainingSet = []
  TestSet = []
  LoadData(Filename, split, TrainingSet, TestSet)

  Predictions = []
  for testinstance in TestSet:
    Neighbors = GetNeighbors(TrainingSet, testinstance, k)
    Responses = GetResponse(Neighbors)
    Predictions.append(Responses)

  Accuracy = GetAccuracy(TestSet, Predictions)
  print 'Accuracy:', str(round(Accuracy,2))+'%'

  end = time.clock()
  print 'Time:', end-start, 's'


### Actually run program. I modified it to loop over values of k (, split).
'''
mink = 2
Loops = 1
for k in xrange(mink,mink+Loops):
  main(k,Split)
'''

### Run it and benchmark it
import profile
profile.run('main(k,Split)')
