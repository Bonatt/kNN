### Employing a kNN Algorithm for Surface Alpha Event Discrimination in a Toy DEAP-3600 Simulation

The [DEAP-3600](http://deap3600.ca/) is a direct detection dark matter experiment. 
A 1.7 m ID acrylic sphere holds approximately 3600 kg of liquid argon:

<img src="DEAP3600Detector.png", width="368", height="494" />

I spent almost two years during my Masters degree researching real GEANT4 simulations of the DEAP-3600 detector.
Post-Masters I attempted to create a toy model of this detector, generate several types of events, 
and then classify these events via 
a [k-nearest neighbors (kNN) algorithm](https://en.wikipedia.org/wiki/K-nearest_neighbors_algorithm). 
This was my first dive into machine learning techniques in mid-2017.
I will ignore all hardware aspects other than the "argon" and the "acrylic surface". 
For more information regarding the real detector and its surface events, 
please see [my thesis](https://qspace.library.queensu.ca/handle/1974/15940).

Because I originally created this project without git, the directories within this GitLab respository contain many old files.
I have kept these for legacy and interest sake, despite bloating any available disk space. I apologize.

I simulated three classes of geometric events:
1. Surface (acrylic vessel)
2. Bulk (argon)
3. Neck (acrylic flowguide)

I only focused on geometric effects here -- no energy, no pulseshape, etc. 
Of course a service algorithm could utilize those features.
From here on, the true positions of the simulation will be referred to as Monte Carlo or MC or True,
and the detected positions of the simulation will be referred to as Reconstructed or Reconstruction or R.

This project had two phases: 2D and 3D. Original prototyping was done in 2D and final (prematurely halted) analysis was done in 3D.


### Generation

#### 2D

For the surface events, I uniformly generated some number of events over a ring (2D) or ball (3D).
For bulk events, I uniformly generated them over a disk (2D) or sphere (3D).
For neck events, I uniformly generated them over some small section of a ring (2D) or ball (3D) at the top of the detector:

<!--A note about the neck: the neck events are really simulating the events originating in flowguide at the top of the vessel.
The scintillation light tends to beam downward.-->

```python
# Generate random events in detector bulk.
nB = 1000                                                       # Number of events
rB = np.random.uniform(low=0., high=R, size=nB)                 # Radius of event
thetaB = np.random.uniform(low=0., high=2.*pi, size=nB)         # Angle of event
# Location of points in rectangular coords.

xB = x0 + sqrt(rB)*np.cos(thetaB)
yB = y0 + sqrt(rB)*np.sin(thetaB)
```
```python
### Generate random events on detector surface
nS = nB
thetaS = np.random.uniform(low=0., high=2.*pi, size=nS)

xS = x0 + sqrt(R) * np.cos(thetaS)
yS = y0 + sqrt(R) * np.sin(thetaS)
```
```python
### Generate random events on neck
nN = nB
neckcone = 15*pi/180.
thetaN = np.random.uniform(low=-neckcone, high=neckcone, size=nN)

xN = x0 + sqrt(R) * np.cos(thetaN)
yN = y0 + sqrt(R) * np.sin(thetaN)
```

Below is the true position of these events:

![2D Monte Carlo](ToyDEAP/Generation/cMonteCarlo.png)

The bulk events are shown in purple, surface in grey, and neck in cyan (right). 
The PMTs are gold and a fiducial volume is shown as "pac-man" (center).

After generating the events, I simulated detection of these events. DEAP-3600 is not perfect and often (always)
the estimated origin of an event is skewed. I originally tried to do this by simulating a simple PMT-based centroid model 
(which is the basis for all position reconstruction algorithms in DEAP) but abandoned that method for an easier, 
less realistc smearing effect.

Below is a visualization of that abandoned centroid in action for a single bulk event. 
From memory, I think this centroid actually performed rather accurately -- too accuately to be realistic.

![2D Centroid](ToyDEAP/Generation/cCentroid1.png)

And below is the reconstructed position of events. These were the data used for kNN. 
I could have just generated the events like this in the first place but wanted to create a more realistic experience, 
whatever that means.

![2D Reconstruction](/ToyDEAP/Generation/cReconstruction.png)


But how were the positions of these events "reconstructed"?
```python
### Wiggle bulk events around by some +- sigma value
sigma = 0.15
xB_R = [x+i for x,i in zip(np.random.uniform(-sigma,sigma,nS), xB)]
yB_R = [y+i for y,i in zip(np.random.uniform(-sigma,sigma,nS), yB)]
```

```python
### Wiggle surface events around by some +- sigma value
sigma = 0.05
xS_R = [x+i for x,i in zip(np.random.uniform(-sigma,sigma,nS), xS)]
yS_R = [y+i for y,i in zip(np.random.uniform(-sigma,sigma,nS), yS)]

### Then smear wiggled surface events position by Pareto function. 
# Higher alpha means less leakage. Like 1,2,5,10.
alpha = 5
xS_R = [x/random.paretovariate(alpha) for x in xS]
yS_R = [y/random.paretovariate(alpha) for y in yS]
```

```python
### Wiggle neck events around by some +- sigma value
sigma = 0.05
xN_R = [x+i for x,i in zip(np.random.uniform(-sigma,sigma,nN), xN)]
yN_R = [y+i for y,i in zip(np.random.uniform(-sigma,sigma,nN), yN)]

### Then smear wiggled neck events position by Pareto function. 
alpha = 5
xN_R = [x/random.paretovariate(alpha) for x in xN]
yN_R = [y/random.paretovariate(alpha/10.) for y in yN]
```



#### 3D

For improved realism, a third spatial dimension was added. All generation procedures were similar to above but in 3D instead of 2D:

```python
### Generate random events in detector bulk.
nB = 5000                                            
Br = np.random.uniform(low=0., high=R, size=nB)
Btheta = [i*2*pi for i in np.random.rand(nB)]
Bphi = [np.arccos(2*i-1) for i in np.random.rand(nB)]

Bx = Br**(1/3.) * np.cos(Btheta) * np.sin(Bphi)
By = Br**(1/3.) * np.sin(Btheta) * np.sin(Bphi)
Bz = Br**(1/3.) * np.cos(Bphi)
```
```python
### Generate random events on detector surface
nS = nB
Stheta = [i*2*pi for i in np.random.rand(nS)]
Sphi = [np.arccos(2*i-1) for i in np.random.rand(nS)]
Sx = R**(1/3.) * np.cos(Stheta) * np.sin(Sphi)
Sy = R**(1/3.) * np.sin(Stheta) * np.sin(Sphi)
Sz = R**(1/3.) * np.cos(Sphi)
```
```python
### Generate random events on neck
nN = nB
neckTheta = 360*pi/180.
neckPhi = 15*pi/180.
Ntheta = np.random.uniform(low=-neckTheta, high=neckTheta, size=nN)
Nphi = np.random.uniform(low=-neckPhi, high=neckPhi, size=nN)

Nx = R**(1/3.) * np.cos(Ntheta) * np.sin(Nphi)
Ny = R**(1/3.) * np.sin(Ntheta) * np.sin(Nphi)
Nz = R**(1/3.) * np.cos(Nphi)
```

The below plot now incorporates both these true (left) and reconstruced (right) events in a single projection plot. 
The neck has also been reorientated intuitively towards the top of the plot.

![3D Monte Carlo and Reconstruction](/ToyDEAP/Generation/MonteCarlo-Reconstruction_3D.png)

```python
# Wiggle bulk
sigma = 0.15
Bx_R = [x+i for x,i in zip(np.random.uniform(-sigma,sigma,nB), Bx)]
By_R = [y+i for y,i in zip(np.random.uniform(-sigma,sigma,nB), By)]
Bz_R = [z+i for z,i in zip(np.random.uniform(-sigma,sigma,nB), Bz)]
```

```python
# Wiggle, smear surf
sigma = 0.05
Sx_R = [x+i for x,i in zip(np.random.uniform(-sigma,sigma,nS), Sx)]
Sy_R = [y+i for y,i in zip(np.random.uniform(-sigma,sigma,nS), Sy)]
Sz_R = [z+i for z,i in zip(np.random.uniform(-sigma,sigma,nS), Sz)]

alpha = 5
Sx_R = [x/random.paretovariate(alpha) for x in Sx]
Sy_R = [y/random.paretovariate(alpha) for y in Sy]
Sz_R = [z/random.paretovariate(alpha) for z in Sz]
```

```python
# Wiggle, smear neck
sigma = 0.05
Nx_R = [x+i for x,i in zip(np.random.uniform(-sigma,sigma,nN), Nx)]
Ny_R = [y+i for y,i in zip(np.random.uniform(-sigma,sigma,nN), Ny)]
Nz_R = [z+i for z,i in zip(np.random.uniform(-sigma,sigma,nN), Nz)]

alpha = 5
Nx_R = [x/random.paretovariate(alpha/10.) for x in Nx]
Ny_R = [y/random.paretovariate(alpha/10.) for y in Ny]
Nz_R = [z/random.paretovariate(alpha) for z in Nz]
```



All events that were reconstructed outside the detector were just deleted.
Surviving events were saved to file for classification 
as "x, y, class" for 2D and "x, y, z, class" for 3D:

```
head ToyDEAP_8791-9000.data

-0.159701601051,0.629384590285,bulk
0.33077410057,-0.162913628906,bulk
0.500569705867,0.678823158081,bulk
...
```

```
head ToyDEAP_8651-9000_3D.data

-0.193035661278,-0.123391246406,-0.203975510153,bulk
-0.0313243946923,0.696073843605,0.0620695845755,bulk
0.250972055521,0.0800208815532,0.683250076419,bulk
...
```



### Classification

From here on I will solely focus on the analysis of the 3D data.

For an introduction in kNN algorithms, I followed a tutorial 
by [Jason Brownlee](http://machinelearningmastery.com/tutorial-to-implement-k-nearest-neighbors-in-python-from-scratch/). 
It used a susbet the famous iris dataset. 
I then reimplemented his code completely in my own vectorized format, 
but for whatever reason it ran almost an order of magnitude slower. 
I went back to the bones of his code, which became to bones of the kNN algorithm used here.
I will not go into detail on how kNN or the code works; you can view the original 
or [my file](https://gitlab.com/Bonatt/kNN/blob/master/ToyDEAP/Classification/ToyDEAP.py).
In short, view the below infographic:

![kNN Infographic](kNNInfographic.jpg)

There are, however, several parameters that determine the efficacy of my kNN algorithm:
1. The number of nearest neighbors, k
2. The distance metric used to determine neighbors
3. The weight, if any, applied to each vote
4. The mimimum threshold, if any, necessary for a neighbor to be classified (as bulk)

There exists an interplay between these parameters when attempting to accurately classify test events.

A general rule of thumb says that k should be sqrt(i) or sqrt(i)/2 
where i is the number of instances in the training set. 
Here, the total number is instances (traing + test) is bout 2900.
Also, k should be odd to minimize dilemmas. Here, k was about 27 after a 66% split.
The distance metric used can speed up computations and/or increase real-world accuracy. 
I don't think the choice between Euclidean and Manhattan here made a significant difference.
A weight may be applied to votes according the their distance from the test point in question; the closer the neighbors, 
the more weight that neighbors has in determining the class. This is often 1/d or 1/d^2.
The minimum threshold is called "cleanbulk" within the code.
It is the minimum percent of true "bulk" neighbors necessary for the test point to also be predicted "bulk". 
In DEAP, the aim is maximize clean livetime by minimizing leakage from nonbulk events. 
Or in other words, DEAP wants to correctly reject all nonbulk events while retaining as many bulk events as possible. 
Thus to increase accuracy the total number of bulk events collected (for use in some imaginary future dark matter analysis) 
will decrease as this threshold increases. 
PS: cuts on events reconstructed outside the fiducial volume, e.g., is one of many other solutions.

Instead of pasting all plots here with weight (w) on/off and a threshold (bn) of {0,50,75,90,100}%,
I will only show w=0 and bn=0 below.
For the other plots, please see the [Classification](https://gitlab.com/Bonatt/kNN/tree/master/ToyDEAP/Classification) folder.
For a table of all results, see the end of this README.
For the outputs of all results, see [outputs_2018.log](/ToyDEAP/Classification/outputs_2018.log) (this was created manually).
Now let's look at this one example...

<!--Below is a set of three plots. 
The 2892-event dataset was split into 67% training and 33% test. The numb-->

```
Data: ../Generation/ToyDEAP_2892-3000_3D.data
Features: 3
Data set: 2892
Split: 0.67
Training set: 1956
Test set: 936
k: 27
Metric: e
Weight: 0
Clean bulk: 0%
00% done
...
90% done
Accuracy: 73.61%
Runtime: 2.890625 s

Captured 205/282 (72.7%) of bulk events with an accuracy of 128/205 (62.44%).
```

With the above parameters ("Metric: e" == Euclidian; "Weight: 0" == no weighting function; "Clean bulk: 0%" == no minimum threshold),
the below set of three plots was generated. 


![Geometric w=0](/ToyDEAP/Classification/Geometric_set=2892_s=0.67_k=27_m=e_w=0_bn=0_2018.png)

The left plot shows the full training set on the left (small black dots) and fill test set on the right (large black dots).
The middle plot shows the true (reconstructed) class on the left and the predicted class on the right.
The right plot shows the correctly-labelled events on the right (green dots) and incorrectly-labelled events on the left (red dots).

There is a bit of information that we can glean from this. We can see that the kNN algorithm has an accuracy of about 73%.
We can also see what specifically contributes to this accuracy: neck events are almost always correctly classified, 
and there tends to be poor discrimination between surface and bulk events. 
This makes sense because (incorrectly, realistlcally) the density of the neck events is very high, creating a population for which
almost all events within that region are likely neck events. I believe that if the number of events of each class 
was proper -- say, 90% bulk, 8% surface, and 2% neck -- that the results would be much different 
(and better for bulk event identification).

There is a common method of visually determining classification discrimination: the confusion matrix.
The below infographic explains the type of visualization.

![Confusion Matrix Infographic](ConfusionMatrixInfographic.jpg)

Note: I am unsure of the conventionis when generating confusion matrices. Here is my attempt: 

![Confusion w=0](/ToyDEAP/Classification/Confusion_set=2892_s=0.67_k=27_m=e_w=0_bn=0_2018.png)

This is saying that 92.3% of predicted neck events (pNeck) are truly neck events (tNeck), 
with 4.4% being truly surface events (tSurf) and 3.3% being truly bulk events (tBulk). Neck events are accurately classified.

Conversely, only 62% of predicted surface events (pSurf) are truly surface events, and only 62% of predicted bulk events (pBulk)
are truly bulk events. Surface and bulk events are not accurately classified, and are often "confused" for one another.
As mentioned above, generating with proper statistics may yield better accuracy here.

How does setting a threshold contibute to the results? The below table contains results for nonweighted voting.

| bn = | 0 | 50 | 75 | 90 | 100 |
| -- | -- | -- | -- | -- | -- |
| Total accuracy | 73.61% | 73.51% | 71.37% | 69.03% | 67.67& |
| Bulk, captured | 205/282 (72.7%) | 188/302 (62.25%) | 37/283 (13.07%) | 12/291 (4.12%) | 2/302 (0.66%) |
| Bulk, accuracy | 128/205 (62.44%) | 130/188 (69.15%) | 35/37 (94.59%) | 12/12 (100.0%) | 2/2 (100.0%) |

"Total accuracy" is the accuracy for all event types.
"Bulk, captured" is the percentage of bulk events that met the threshold requirement.
"Bulk, accuracy" is the the accuracy of the bulk events that met the threshold requirement.

Total accuracy decreases as bn increases because if the threshold is not met, the event is automatically classified as "surface" 
(to kind of just get rid of it). The goal here was to create as many guaranteed-bulk events as possible.
If there is no threshold minimum (bn = 0), the accuracy of 205 events is only 62.44%.
However, if there is a threshold of 75%, the accuracy of only 35 events is 94.59%.
Somewhere between 75% and 90% threshold would yield a maximum intersection between accuracy and number of events (i.e. livetime).

I didn't do much analysis beyhond this but there is certainly infinite things to do. It's physics.
