import ROOT
import numpy as np
import math as math
import os
import linecache
import sys
import random


# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return np.sqrt(x)
# Redefine pi to something shorter
pi = np.pi    

#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();
print ''




### Inspired by DEAP-3600 surface background problem.
# I've done this before via GEANT4 Monte Carlo (and some real data), but let's try real kNN with a toy model:
# 2D only.
# I. For surface background events:
#   i. Generate events (uniformly) on a circle (MC true surface).
#   ii. Randomly move them around, preferably preferentially towards the center (Reconstruction) 
# II. For bulk events:
#   i. Generate events (uniformly) on disk (MC true bulk).
#   ii. Optional. Randomly move them around. This doesn't really effect much, but is akin to more reconstruction analysis.
# III. With this training data, use kNN to parse these points into surface/bulk.
# IV. Generate new data for it try.
#
# V. Generate neck events.
#
# Much of this code was pulled from AvgDistInCircle
#



### DEAP-3600 parameters. May not be used much.
DEAPRadius = 0.85                                                       # m
DEAPSurfaceArea = 4.*pi*DEAPRadius**2                                # m2, roughly outside
DEAPVolume = 4./3.*pi*DEAPRadius**3                                  # m3
DEAPCircumference = 2.*pi*DEAPRadius





# Define unit circle parameters centered at (0,0).
R = 1.								# Radius of detector
x0 = 0.
y0 = 0.
z0 = 0.



###### Create various interesting aspects regarding detector ######
'''
### Draw neck of detector
gNeck = ROOT.TGraph2D(1, np.array(R), np.array(y0), np.array(z0))
gNeck.SetMarkerStyle(ROOT.kCircle)
gNeck.SetMarkerColor(ROOT.kGray+2)
gNeck.SetMarkerSize(1)

### Generate "PMT" locations on detector surface
nPMT = 255                                                       # Counted lightguides in standard cut drawing instead.
print ' ', nPMT, 'PMTs.'
# There used to be an offset here to simulate gap in PMTs at neck. It made centroid not work. May be interesting to visit again.
# From http://www.geom.uiuc.edu/docs/reference/CRC-formulas/node42.html
thetaPMT = [i*2*pi/nPMT-2*pi/nPMT/2. for i in range(nPMT)] 	# Angle of PMT with evenly spaced nPMTs
phiPMT = [(i*2*pi/nPMT-2*pi/nPMT/2.)/2. for i in range(nPMT)]
xPMT = x0 + sqrt(R) * np.cos(thetaPMT) * np.sin(phiPMT)
yPMT = y0 + sqrt(R) * np.sin(thetaPMT) * np.sin(phiPMT)
zPMT = z0 + sqrt(R) * np.cos(phiPMT)
# Graph for PMTs
gPMT = ROOT.TGraph2D(len(xPMT), xPMT, yPMT, zPMT)
gPMT.GetXaxis().SetLimits(-R, R)
gPMT.GetYaxis().SetRangeUser(-R, R)
gPMT.SetMarkerStyle(ROOT.kFullDotMedium)
gPMT.SetMarkerColor(ROOT.kYellow-6)
gPMT.SetMarkerSize(1)
'''
### Fiducial volume curve
# From PostAnalyze.C
# if (TMath::ATan(z_mb/r2_mb) < 55.0*TMath::Pi()/180.0  &&  r3_mb < 591.0){...}
Fiducial = ROOT.TArc()
Fiducial.SetLineColor(ROOT.kGray)
Fiducial.SetFillStyle(4000)
FiducialR = 0.6 #591
angle1 = 145
angle2 = 395




BulkColor = ROOT.kViolet#+1 #+1
SurfColor = ROOT.kGray+1 #+2
NeckColor = ROOT.kCyan#+1



###### MONTE CARLO OF BULK, SURFACE EVENTS ######

# Generate random events in detector bulk.
nB = 5000							# Number of events
print ' ', nB, 'bulk events.'
Br = np.random.uniform(low=0., high=R, size=nB)			# Radius of event
#Btheta = np.random.uniform(low=0., high=2.*pi, size=nB)		# Angle of event. Zero theta is far right on this plot.
#Bphi = np.random.uniform(low=0., high=pi, size=nB)         # Angle of event. Zero theta is far right on this plot.
Btheta = [i*2*pi for i in np.random.rand(nB)]
#Bphi = [i*pi for i in np.random.rand(nB)]
# From http://mathworld.wolfram.com/SpherePointPicking.html
Bphi = [np.arccos(2*i-1) for i in np.random.rand(nB)]

# Location of points in rectangular coords.
'''
Bx = sqrt(Br) * np.cos(Btheta) * np.sin(Bphi)
By = sqrt(Br) * np.sin(Btheta) * np.sin(Bphi)
Bz = sqrt(Br) * np.cos(Bphi)
'''
'''
Bx = Br * np.cos(Btheta) * np.sin(Bphi)
By = Br * np.sin(Btheta) * np.sin(Bphi)
Bz = Br * np.cos(Bphi)
'''
# https://math.stackexchange.com/questions/87230/picking-random-points-in-the-volume-of-sphere-with-uniform-probability
# 
Bx = Br**(1/3.) * np.cos(Btheta) * np.sin(Bphi)
By = Br**(1/3.) * np.sin(Btheta) * np.sin(Bphi)
Bz = Br**(1/3.) * np.cos(Bphi)

Br2 = sqrt(Bx**2+By**2)

# Save new arrays to delete along with reconstructed events outside detector. Must be lists to del later.
'''
Bx2 = list(Bx[:])
By2 = list(By[:])
Bz2 = list(Bz[:])
'''

# Histo for bulk events
nbins = 200 #int(R*850./10.) #200
hB3D = ROOT.TH3D('hB3D','hB3D', nbins,-R,R, nbins,-R,R, nbins,-R,R)
hB3D.SetMarkerStyle(ROOT.kFullDotMedium)
hB3D.SetMarkerColor(BulkColor)

hB2D = ROOT.TH2D('hB2D','hB2D', nbins,-R,R, nbins,-R,R)
hB2D.SetMarkerStyle(ROOT.kFullDotMedium)
hB2D.SetMarkerColor(BulkColor)




### Generate random events on detector surface
nS = nB
print ' ', nS, 'surface events.'
#Stheta = np.random.uniform(low=0., high=2.*pi, size=nS)		# Angle of event, radius = R
#Sphi = np.random.uniform(low=0., high=pi, size=nS)         # Angle of event. Zero theta is far right on this plot.
Stheta = [i*2*pi for i in np.random.rand(nS)]
#Sphi = [i*pi for i in np.random.rand(nS)]
Sphi = [np.arccos(2*i-1) for i in np.random.rand(nS)]
Sx = R**(1/3.) * np.cos(Stheta) * np.sin(Sphi)
Sy = R**(1/3.) * np.sin(Stheta) * np.sin(Sphi)
Sz = R**(1/3.) * np.cos(Sphi)
Sr2 = sqrt(Sx**2+Sy**2)

# Save new arrays to delete along with reconstructed events outside detector. Must be lists to del later.
'''
Sx2 = list(Sx[:])
Sy2 = list(Sy[:])
Sz2 = list(Sz[:])
'''

# Histo for surf events
hS3D = ROOT.TH3D('hS3D', 'hS3D', nbins,-R,R, nbins,-R,R, nbins,-R,R)
hS3D.SetMarkerColor(SurfColor)
hS3D.SetMarkerStyle(ROOT.kFullDotMedium)
hS3D.SetMarkerColor(SurfColor)

hS2D = ROOT.TH2D('hS2D','hS2D', nbins,-R,R, nbins,-R,R)
hS2D.SetMarkerStyle(ROOT.kFullDotMedium)
hS2D.SetMarkerColor(SurfColor)





### Generate random events on neck
nN = nB
print ' ', nN, 'neck events.'
neckTheta = 360*pi/180.
neckPhi = 15*pi/180.
Ntheta = np.random.uniform(low=-neckTheta, high=neckTheta, size=nN)         # Angle of event, radius = R
Nphi = np.random.uniform(low=-neckPhi, high=neckPhi, size=nN)         # Angle of event, radius = R
#Nphi = [np.arccos(2*i-1) for i in np.random.rand(nN)]
'''
Nx = sqrt(R) * np.cos(Ntheta) * np.sin(Nphi)
Ny = sqrt(R) * np.sin(Ntheta) * np.sin(Nphi)
Nz = sqrt(R) * np.cos(Nphi)
'''
'''
Nx = R * np.cos(Ntheta) * np.sin(Nphi)
Ny = R * np.sin(Ntheta) * np.sin(Nphi)
Nz = R * np.cos(Nphi)
'''
# From https://stackoverflow.com/questions/5408276/sampling-uniformly-distributed-random-points-inside-a-spherical-volume
Nx = R**(1/3.) * np.cos(Ntheta) * np.sin(Nphi)
Ny = R**(1/3.) * np.sin(Ntheta) * np.sin(Nphi)
Nz = R**(1/3.) * np.cos(Nphi)
Nr2 = sqrt(Nx**2+Ny**2)

# Save new arrays to delete along with reconstructed events outside detector. Must be lists to del later.
'''
Nx2 = list(Nx[:])
Ny2 = list(Ny[:])
Nz2 = list(Nz[:])
'''

# Histo for surf events
hN3D = ROOT.TH3D('hN3D', 'hN3D', nbins,-R,R, nbins,-R,R, nbins,-R,R)
hN3D.SetMarkerStyle(ROOT.kFullDotMedium)
hN3D.SetMarkerColor(NeckColor)

hN2D = ROOT.TH2D('hN2D','hN2D', nbins,-R,R, nbins,-R,R)
hN2D.SetMarkerStyle(ROOT.kFullDotMedium)
hN2D.SetMarkerColor(NeckColor)



### Fill histos
for bx,by,bz,br2, sx,sy,sz,sr2, nx,ny,nz,nr2 in zip(Bx,By,Bz,Br2, Sx,Sy,Sz,Sr2, Nx,Ny,Nz,Nr2):
  hB3D.Fill(bx, by, bz)
  hS3D.Fill(sx, sy, sz)
  hN3D.Fill(nx, ny, nz)

  hB2D.Fill(-br2, bz)
  hS2D.Fill(-sr2, sz)
  hN2D.Fill(-nr2, nz)


### Project 3D histo into 2D. Yes, (2D weighted with z) and (3D proj) are same thing.
hB3D_proj = hB3D.Project3D('zx') # zx == zy for x/y symmetry.
hS3D_proj = hS3D.Project3D('zx')
hN3D_proj = hN3D.Project3D('zx')




### Plot bulk, surface, neck events as true Monte Carlo
cMonteCarlo3D = ROOT.TCanvas('cMonteCarlo3D', 'cMonteCarlo3D', 720, 720)
hB3D_proj.SetTitle('Monte Carlo: nB='+str(nB)+', nS='+str(nS)+', nN='+str(nN)+';x, y;z')
hB3D_proj.SetStats(0)
hB3D_proj.Draw()
Fiducial.DrawArc(0,0,FiducialR,angle1,angle2)
hS3D_proj.Draw('same')
hN3D_proj.Draw('same')
#cMonteCarlo.SaveAs('MonteCarlo_3D.png')
#hB3D_proj = hB3D.Project3D('y'); hB3D_proj.Draw()




### Plot bulk, surface, neck events as true Monte Carlo
cMonteCarlo2D = ROOT.TCanvas('cMonteCarlo2D', 'cMonteCarlo2D', 720, 720)
hB2D.SetTitle('Monte Carlo: nB='+str(nB)+', nS='+str(nS)+', nN='+str(nN)+';#sqrt{x^{2}+y^{2}};z')
hB2D.SetStats(0)
hB2D.Draw()
Fiducial.DrawArc(0,0,FiducialR,angle1,angle2)
hS2D.Draw('same')
hN2D.Draw('same')











### Smear...
# Wiggle bulk
sigma = 0.15
Bx_R = [x+i for x,i in zip(np.random.uniform(-sigma,sigma,nB), Bx)]
By_R = [y+i for y,i in zip(np.random.uniform(-sigma,sigma,nB), By)]
Bz_R = [z+i for z,i in zip(np.random.uniform(-sigma,sigma,nB), Bz)]

Br_R = [sqrt(x**2+y**2+z**2) for x,y,z in zip(Bx_R,By_R,Bz_R)]
deleted = 0
for i,r in enumerate(Br_R):
  if (abs(r) > R):
    del Bx_R[i-deleted], By_R[i-deleted], Bz_R[i-deleted]
    #del Bx2[i-deleted], By2[i-deleted], Bz2[i-deleted]          # Also delete this do when comparing MC vs R, event IDs match.
    deleted = deleted + 1

print ' ', len(Bx_R), 'bulk events reconstructed within the detector.'

Br2_R = sqrt(np.array(Bx_R)**2+np.array(By_R)**2)

hB2D_R = ROOT.TH2D('hB2D_R','hB2D_R', nbins,-R,R, nbins,-R,R)
hB2D_R.SetMarkerStyle(ROOT.kFullDotMedium)
hB2D_R.SetMarkerColor(BulkColor)




# Wiggle, smear surf
sigma = 0.05
Sx_R = [x+i for x,i in zip(np.random.uniform(-sigma,sigma,nS), Sx)]
Sy_R = [y+i for y,i in zip(np.random.uniform(-sigma,sigma,nS), Sy)]
Sz_R = [z+i for z,i in zip(np.random.uniform(-sigma,sigma,nS), Sz)]

alpha = 5
Sx_R = [x/random.paretovariate(alpha) for x in Sx]
Sy_R = [y/random.paretovariate(alpha) for y in Sy]
Sz_R = [z/random.paretovariate(alpha) for z in Sz]

Sr_R = [sqrt(x**2+y**2+z**2) for x,y,z in zip(Sx_R,Sy_R,Sz_R)]
deleted = 0
for i,r in enumerate(Sr_R):
  if (abs(r) > R):
    del Sx_R[i-deleted], Sy_R[i-deleted], Sz_R[i-deleted]
    #del Sx2[i-deleted], Sy2[i-deleted], Sz2[i-deleted]          # Also delete this do when comparing MC vs R, event IDs match.
    deleted = deleted + 1

print ' ', len(Sx_R), 'surface events reconstructed within the detector.'

Sr2_R = sqrt(np.array(Sx_R)**2+np.array(Sy_R)**2)

hS2D_R = ROOT.TH2D('hS2D_R','hS2D_R', nbins,-R,R, nbins,-R,R)
hS2D_R.SetMarkerStyle(ROOT.kFullDotMedium)
hS2D_R.SetMarkerColor(SurfColor)





# Wiggle neck
sigma = 0.05
Nx_R = [x+i for x,i in zip(np.random.uniform(-sigma,sigma,nN), Nx)]
Ny_R = [y+i for y,i in zip(np.random.uniform(-sigma,sigma,nN), Ny)]
Nz_R = [z+i for z,i in zip(np.random.uniform(-sigma,sigma,nN), Nz)]

### Then smear wiggled neck events position by Pareto function. Higher alpha means less leakage. Like 1,2,5,10. 
alpha = 5
Nx_R = [x/random.paretovariate(alpha/10.) for x in Nx]
Ny_R = [y/random.paretovariate(alpha/10.) for y in Ny]
Nz_R = [z/random.paretovariate(alpha) for z in Nz]


Nr_R = [sqrt(x**2+y**2+z**2) for x,y,z in zip(Nx_R,Ny_R,Nz_R)]
deleted = 0
for i,r in enumerate(Nr_R):
  if (abs(r) > R):
    del Nx_R[i-deleted], Ny_R[i-deleted], Nz_R[i-deleted]
    #del Nx2[i-deleted], Ny2[i-deleted], Nz2[i-deleted]          # Also delete this do when comparing MC vs R, event IDs match.
    deleted = deleted + 1

print ' ', len(Nx_R), 'neck events reconstructed within the detector.'

Nr2_R = sqrt(np.array(Nx_R)**2+np.array(Ny_R)**2)

hN2D_R = ROOT.TH2D('hN2D_R','hN2D_R', nbins,-R,R, nbins,-R,R)
hN2D_R.SetMarkerStyle(ROOT.kFullDotMedium)
hN2D_R.SetMarkerColor(NeckColor)





### Fill histos
for bz,br2, sz,sr2, nz,nr2 in zip(Bz_R,Br2_R, Sz_R,Sr2_R, Nz_R,Nr2_R):
  hB2D_R.Fill(br2, bz)
  hS2D_R.Fill(sr2, sz)
  hN2D_R.Fill(nr2, nz)




### Plot bulk, surface, neck events as true Monte Carlo
hB2D_R.Draw('same')
hS2D_R.Draw('same')
hN2D_R.Draw('same')

cMonteCarlo2D.SaveAs('MonteCarlo-Reconstruction_3D.png')





with open('ToyDEAP_'+str(len(Bx_R+Sx_R+Nx_R))+'-'+str(nB+nS+nN)+'_3D.data', 'w') as file:
  for x,y,z in zip(Bx_R,By_R,Bz_R):
    file.write(str(x)+',')
    file.write(str(y)+',')
    file.write(str(z)+',')
    file.write('bulk\n')

  for x,y,z in zip(Sx_R,Sy_R,Sz_R):
    file.write(str(x)+',')
    file.write(str(y)+',')
    file.write(str(z)+',')
    file.write('surf\n')

  for x,y,z in zip(Nx_R,Ny_R,Nz_R):
    file.write(str(x)+',')
    file.write(str(y)+',')
    file.write(str(z)+',')
    file.write('neck\n')

