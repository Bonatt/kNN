import ROOT
import numpy as np
import math as math
import os
import linecache
import sys

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()...
def sqrt(x):
  return np.sqrt(x)

#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();
print ''




### Inspired by DEAP-3600 surface background problem.
# I've done this before via GEANT4 Monte Carlo (and some real data), but let's try real kNN with a toy model:
# 2D only.
# I. For surface background events:
#   i. Generate events (uniformly) on a circle (MC true surface).
#   ii. Randomly move them around, preferably preferentially towards the center (Reconstruction) 
# II. For bulk events:
#   i. Generate events (uniformly) on disk (MC true bulk).
#   ii. Optional. Randomly move them around. This doesn't really effect much, but is akin to more reconstruction analysis.
# III. With this training data, use kNN to parse these points into surface/bulk.
# IV. Generate new data for it try.


DEAPRadius = 0.85                                                       # m
DEAPSurfaceArea = 4.*np.pi*DEAPRadius**2                                # m2, roughly outside
DEAPVolume = 4./3.*np.pi*DEAPRadius**3                                  # m3
DEAPCircumference = 2.*np.pi*DEAPRadius





### From AvgDistInCircle
# Unit circle centered at (0,0).
R = 1.								# Radius of detector
x0 = 0.
y0 = 0.





### Draw center of detector
'''
g0 = ROOT.TGraph(1, np.array(x0), np.array(y0))
g0.SetMarkerStyle(ROOT.kFullDotLarge)
g0.SetMarkerSize(0.25)
'''
### Draw neck of detector
#NeckRadius = 0.254/2.						# Neck is 10 inches in diameter. 10 in = 0.254 m
gN = ROOT.TGraph(1, np.array(R), np.array(y0))
gN.SetMarkerStyle(ROOT.kCircle)
gN.SetMarkerSize(1)#DEAPRadius/NeckRadius)

### Generate "PMT" locations on detector surface
#PMTRadius = 0.2032/2.                                          # m
#PMTSurfaceArea = 4.*np.pi*(0.2032/2.)**2                       # m2, letting diameter of PMT be 8 in = 0.2032 m
# http://mathworld.wolfram.com/SphericalSegment.html
# Okay, screw trying to calculate some slice or dA that 255 PMTs would take up, thus getting some nPMT for on the equator...
nPMT = 19                                                       # Counted lightguides in standard cut drawing instead.
neckoffset = 30.*np.pi/180.					# Degrees*np.pi/180 = rad
print ' ', nPMT, 'PMTs.'
thetaPMT = [i*((2*np.pi-neckoffset)/nPMT)+neckoffset for i in range(nPMT)] # angle of PMT. From 30 degrees, evenly spaced nPMTs
xPMT = x0 + sqrt(R) * np.cos(thetaPMT)
yPMT = y0 + sqrt(R) * np.sin(thetaPMT)
# Graph for PMTs
gPMT = ROOT.TGraph(len(xPMT), xPMT, yPMT)
gPMT.GetXaxis().SetLimits(-R, R)
gPMT.GetYaxis().SetRangeUser(-R, R)
gPMT.SetMarkerStyle(ROOT.kFullDotLarge)
gPMT.SetMarkerColor(ROOT.kYellow-6)
gPMT.SetMarkerSize(1)#DEAPRadius/PMTRadius)

### Fiducial volume
#Fiducial = ROOT.TEllipse(x0,y0,R*0.75,R*0.75)
#Fiducial.SetFillStyle(0)
#Fiducial.Draw('same')
# From PostAnalyze.C
Fiducial = ROOT.TArc()
Fiducial.SetLineColor(ROOT.kGray)
Fiducial.SetFillStyle(4000)
# if (TMath::ATan(z_mb/r2_mb) < 55.0*TMath::Pi()/180.0  &&  r3_mb < 591.0){
FiducialR = 0.6 #591



###### MONTE CARLO OF BULK, SURFACE EVENTS

# Generate random events in detector bulk.
nB = 1000                                                         # number of events
print ' ', nB, 'bulk events.'
# Note that np.random.rand(n) generates points gathered at (0,0), while np.random.uniform(0,R,n) spreads them out ~evenly.
# For simulating a 2D projection of a sphere, np.random.rand() may be more realistic
rB = np.random.uniform(low=0., high=R, size=nB)                   # radius of event
thetaB = np.random.uniform(low=0., high=2.*np.pi, size=nB)        # angle of event. Zero theta is far right on this plot.

# Location of points in rectangular coords.
xB = x0 + sqrt(rB)*np.cos(thetaB)
yB = y0 + sqrt(rB)*np.sin(thetaB)

# Graph for bulk events
gB = ROOT.TGraph(len(xB), xB, yB)
gB.GetXaxis().SetLimits(-R, R)
gB.GetYaxis().SetRangeUser(-R, R)
gB.SetMarkerStyle(ROOT.kFullDotLarge)
gB.SetMarkerColor(ROOT.kViolet+1)
gB.SetMarkerSize(0.25)


### Generate random events on detector surface
nS = nB
print ' ', nS, 'surface events.'
thetaS = np.random.uniform(low=0., high=2.*np.pi, size=nS)        # angle of event, radius = R
xS = x0 + sqrt(R) * np.cos(thetaS)
yS = y0 + sqrt(R) * np.sin(thetaS)

# Graph for surface events
gS = ROOT.TGraph(len(xS), xS, yS)
gS.GetXaxis().SetLimits(-R, R)
gS.GetYaxis().SetRangeUser(-R, R)
gS.SetMarkerStyle(ROOT.kFullDotLarge)
gS.SetMarkerColor(ROOT.kGray+2)
gS.SetMarkerSize(0.25)


### Plot events as true Monte Carlo
cT = ROOT.TCanvas('cT', 'cT', 720, 720)
gB.SetTitle('True: nB='+str(nB)+', nS='+str(nS)+';;')#;X (au);Y (au)')
gB.Draw('ap')
#g0.Draw('same p')
gS.Draw('same p')
Fiducial.DrawArc(0,0,FiducialR,145-90,395-90)
gN.Draw('same p')
gPMT.Draw('same p')
cT.SaveAs('True.png')





###### "RECONSTRUCTION" OF BULK, SURFACE EVENTS

### Fake centroid reconstruction from PMTs. Single event.
# Highlight single event. Just pick index 1. Why not 0? Because single event is ONE event........
xB_1 = xB[1]
yB_1 = yB[1]
gB_1 = ROOT.TGraph(1, np.array(xB_1), np.array(yB_1))
gB_1.SetMarkerStyle(ROOT.kCircle)
gB_1.SetMarkerColor(ROOT.kViolet+1)
gB_1.SetMarkerSize(1)
gB_1.Draw('same p')

# Draw lines from event to PMTs
line = ROOT.TLine()
line.SetLineColor(ROOT.kGray+1)
line.SetLineStyle(ROOT.kDashed)

#Centroid_1 = []
xCentroid_1 = []
yCentroid_1 = []
for x,y in zip(xPMT,yPMT):
  
  xdelta = xB_1-x
  ydelta = yB_1-y
  r = sqrt( (xdelta)**2 + (ydelta)**2 )
  intensity = 1/r**2

  xCentroid_1.append(xdelta)#*intensity)
  yCentroid_1.append(ydelta)#*intensity)
  #Centroid_1.append(r*intensity)
  line.DrawLine(xB_1, yB_1, x, y)

### Plot Reconstructed event position for a single point
xB_R_1 = np.mean(xCentroid_1)
yB_R_1 = np.mean(yCentroid_1)

gB_R_1 = ROOT.TGraph(1, np.array(xB_R_1), np.array(yB_R_1))
gB_R_1.SetMarkerStyle(ROOT.kFullStar)
gB_R_1.SetMarkerColor(ROOT.kViolet+1)
gB_R_1.SetMarkerSize(1)
gB_R_1.Draw('same p')
cT.SaveAs('True2.png')


xDelta_1 = abs(xB_R_1-xB_1)
yDelta_1 = abs(xB_R_1-xB_1)
rDelta_1 = sqrt( xDelta_1**2 + yDelta_1**2 )

print ''
print ' DeltaR between True and Reconstructed position of single event:', rDelta_1
print ' This is only wrong because it always skews the point towards the neck (lack of PMTs). Number of PMT does not affect DeltaR'
print ''




### Fake centroid reconstruction from PMTs. All events.
xB_R = []#np.mean(xCentroid)
yB_R = []#np.mean(yCentroid)
for xb,yb in zip(xB,yB):

  xCentroid = []
  yCentroid = []
  for xpmt,ypmt in zip(xPMT,yPMT):

    xdelta = xb-xpmt
    ydelta = yb-ypmt
    #r = sqrt( (xDelta)**2 + (yDelta)**2 )
    #intensity = 1/r**2

    xCentroid.append(xdelta)
    yCentroid.append(ydelta)

  xB_R.append(np.mean(xCentroid))
  yB_R.append(np.mean(yCentroid))
 
### Plot Reconstructed event position for a single point
#xB_R = np.mean(xCentroid)
#yB_R = np.mean(yCentroid)

gB_R = ROOT.TGraph(len(xB_R), np.array(xB_R), np.array(yB_R))
gB_R.GetXaxis().SetLimits(-R, R)
gB_R.GetYaxis().SetRangeUser(-R, R)
gB_R.SetMarkerStyle(ROOT.kFullStar)
gB_R.SetMarkerColor(ROOT.kViolet+1)
gB_R.SetMarkerSize(1)
#gB_R.Draw('same p')
'''
xDelta = abs(xB_R-xB)
yDelta = abs(xB_R-xB)
rDelta = sqrt( xDelta**2 + yDelta**2 )
'''




######
















### Plot events as Reconstructed
cR = ROOT.TCanvas('cR', 'cR', 720, 720)
gB_R.SetTitle('Reconstructed: nB='+str(nB)+', nS='+str(nS)+';;')
gB_R.Draw('ap')
gS.Draw('same p')
Fiducial.DrawArc(0,0,FiducialR,145-90,395-90)
gN.Draw('same p')
gPMT.Draw('same p')
cR.SaveAs('Reconstructed.png')









#xB_r = [r*i for r,i in zip(np.random.uniform(0.,2.*np.pi,nB), xB)]
