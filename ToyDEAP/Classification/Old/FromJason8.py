import ROOT
import numpy as np
import random
import math
from operator import itemgetter
import time
import csv
from collections import Counter


#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(56); # 51:blue. 52:lo=k/hi=w. 53:lo=r/hi=y. 56:lo=y/hi=r. 54:lo=b/hi=y. 55:lo=b/hi=r, pale rainbow. none:rainbow 
# Palettes, see https://root.cern.ch/doc/master/classTColor.html#C05. Or for ROOT 5.34: https://people.nscl.msu.edu/~noji/colormap
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(1);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();


# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return math.sqrt(x)
# Redefine pi to something shorter
pi = math.pi





### From http://machinelearningmastery.com/tutorial-to-implement-k-nearest-neighbors-in-python-from-scratch/
# Edited to take ToyDEAP3600 data, which I put to file in same manner as iris.data. 
# Rerewriting this code because my first attempt to emulate this code "in my own words" results in worse timing: his ~6 s vs my ~42 s.
# See "FollowingJason" for that attempt.

Filedir = '../Generation/'

Filename = Filedir+'ToyDEAP_2892-3000_3D.data' # Bulk, Surf, Neck; x,y,z
#Filename = Filedir+'ToyDEAP_8651-9000_3D.data' # Bulk, Surf, Neck; x,y,z
#Filename = Filedir+'ToyDEAP_14455-15000_3D.data' # Bulk, Surf, Neck; x,y,z
#Filename = '../../Iris/iris.data'

### How many columns of file are data? And use some percent of data if desired, e.g. 100/100. == 100% of data.
if 'ToyDEAP' in Filename:
  nFeatures = 3
if 'iris' in Filename:
  nFeatures = 4





### Load data, splitting data randomly into training or test sets. And make features from string to float
def LoadData(filename, split):
  # filename = str
  # split = float

  trainingset = []
  testset = []
  with open(filename, 'rb') as csvfile:
    dataset = list(csv.reader(csvfile))
    
    for row in dataset:
      for f in xrange(nFeatures):
        row[f] = float(row[f])

      if random.random() < split:
        trainingset.append(row)
      else:
        testset.append(row)

  return trainingset, testset





### In general, Minkowski distance = # d = ((x-x0)**(p) + ...)**(1/p)
# Manhattan: p = 1
# Euclidean: p = 2 

# Define Euclidean distance.
# r = sqrt( (x-x0)**2 + ... ) for 1D. Add (y-y0)**2 for 2D, etc.
def EuclideanDistance(instance1, instance2):
  # instance1, instance2 = [f1, f2, ..., 'id']
  distance = 0
  for f in xrange(nFeatures):
    distance += (instance1[f] - instance2[f])**2
  return distance #sqrt(distance)

# Define Manhattan distance
def ManhattanDistance(instance1, instance2):
  distance = 0
  for f in xrange(nFeatures):
    distance += abs(instance1[f] - instance2[f])
  return distance




 
### Determine k neighbors. Take in full training dataset and single test instance. 
# Compute distances between single test instance and every training instance (this is looped from main() for every test instance).
# Take k smallest distances.
def GetNeighbors(trainingset, testinstance, k, metric):
  # trainingset = [ [f1, f2, ..., 'id'], [...] ]
  # testinstance = [f1, f2, ..., 'id']
  # k = int

  distances = []
  for traininginstance in trainingset:
    if metric == 'e':
      distance = EuclideanDistance(traininginstance, testinstance)
    if metric == 'm':
      distance = ManhattanDistance(traininginstance, testinstance)
    distances.append((traininginstance, distance))
  distances.sort(key=itemgetter(1))
  ''' 
  neighbors = []
  for d in distances[:k]:
    neighbors.append(d[0])
  return neighbors
  '''
  '''
  neighbors = []
  weights = []
  for d in distances[:k]:
    neighbors.append(d[0])
    weights.append(distances[0][1]/d[1])
  return neighbors, weights 
  ''' 
  #'''
  neighbors = []
  ndistances = []
  for d in distances[:k]:
    neighbors.append(d[0])
    ndistances.append(d[1])
  return neighbors, ndistances
  #'''
  



### Get vote weights
def GetWeights(distances):
  # distances = [d1, d2, ..., dk]

  # Inverse of distance
  #weights = [1/nd for nd in ndistances]

  # Inverse of distance squared
  weights = [1/d/d for d in distances]

  return weights





### Get response, i.e. vote. Assume classification is index [-1] of neighbors. Counts how many times each classification appears. 
def GetResponse(neighbors, weightboolean, distances):
  # neighbors =  [ [f1, f2, ..., 'id'], [...] ]
  # weightboolean = 0, 1 
  # ndistances = [d1, d2, ..., dk]

  responses = [i[-1] for i in neighbors]
  if weightboolean == 0:
    #print Counter(responses).most_common()
    return Counter(responses).most_common()[0][0]

  else:
    '''
    print distances
    weights = GetWeights(distances)
    print weights
    responsesWeighted = []
    for r,w in zip(responses,weights):
      for i in xrange(int(w)):
        responsesWeighted.append(r)
    return Counter(responsesWeighted).most_common(1)[0][0] 
    '''
    classes = {}
    weights = GetWeights(distances)
    for r,w in zip(responses,weights):
     if r in classes: classes[r] += 1*w #int(w)
     else: classes[r] = 1
    #print Counter(classes).most_common()
    return Counter(classes).most_common()[0][0]
 
  '''
  responses = ['surf', 'surf', 'bulk', 'surf', 'bulk', 'bulk', 'surf', 'bulk', 'bulk', 'surf', 'surf', 'surf', 'bulk', 'bulk', 'bulk', 'surf', 'surf', 'surf', 'bulk', 'surf', 'surf', 'surf', 'bulk', 'surf', 'surf', 'bulk', 'surf']

  distances = [0.0017903378560888438, 0.015158888613514665, 0.029335048684445697, 0.031026142686305918, 0.03393932021752905, 0.03396922312129665, 0.039925096782191936, 0.03998517795147122, 0.04768724129820322, 0.049288598120504255, 0.05501469945451865, 0.05501509005737561, 0.05654007327221029, 0.05890176235010384, 0.05931332385136853, 0.061879011398979375, 0.06515698600000999, 0.06520691589550008, 0.06745726185209679, 0.06866656677484315, 0.07275633424212816, 0.07345732669728623, 0.07844866137473024, 0.07899668316684003, 0.08032826242270494, 0.08355111844546466, 0.08887591647282012]

 weights = [311982.33950627135, 4351.763411183088, 1162.0541697449296, 1038.8298711845086, 868.1479041648977, 866.6201261016616, 627.347316806148, 625.4634465966607, 439.739624733653, 411.6300463720545, 330.40188052662205, 330.39718888801167, 312.8147810539335, 288.232813976932, 284.24672908618913, 261.1639729762926, 235.5472436648256, 235.18665778616534, 219.75693150633364, 212.08469660883287, 188.91149334553108, 185.3231876236431, 162.49085556441187, 160.24418775009147, 154.97557425373006, 143.25027495211882, 126.59944907000231]


  for r,w in zip(responses,weights):
   if r in classs: classs[r] +=1
   else: classs[r] = 1
  '''








'''
### Get weighted response
def GetResponseWeighted(neighbors, weights):
  # neighbors =  [ [f1, f2, ..., 'id'], [...] ] 
  # weights = [w1, w2, ... wk]

  responses = [i[-1] for i in neighbors]

  responsesWeighted = []
  for r,w in zip(responses,weights):
    for i in xrange(int(w)):
      responsesWeighted.append(r)

  return Counter(responsesWeighted).most_common(1)[0][0]
'''




### Only call bulk if n of k neighbors are also bulk. Otherwise call 'surf' (for now).
def GetResponseCleanBulk(neighbors,cleanbulk):
  # neighbors =  [ [f1, f2, ..., 'id'], [...] ]; cleanbulk = int
  
  responses = [i[-1] for i in neighbors]

  response = Counter(responses).most_common(1)[0][0]
  n = Counter(responses).most_common(1)[0][1]

  if response == 'bulk':
    if n/float(len(responses)) >= cleanbulk/100.:
      #print ' '*8*k+'^ ALL BULK'
      #print ' '*10+'^ ALL BULK'
      return response
    else:
      return 'surf'
  else:
    return response
      



### How accurate was response? This could be done in above loop...? 
def GetAccuracy(testset, predictions):
  # testset = [ [f1, f2, ..., 'id'], [...] ]
  # predictions = [ 'id', 'id', ... ] 
  
  Correct = []
  Incorrect = []
  correct = 0
  for t,p in zip(testset, predictions):
    if t[-1] == p:
      correct += 1
      Correct.append(t)
    else:
      Incorrect.append(t)

  return (correct/float(len(testset))) * 100.0, Correct, Incorrect

 


 
### Want four plots:
# 1. Full dataset: training and test on same plot
# 2. Test set: predictions
# 3. Test set: actual
# 4. Test set: correct/incorrect as green/red.

def Plot(TrainingSet, TestSet, Predictions, Correct, Incorrect, k, metric, weightboolean, cleanbulk):

  R = 1.0
  BulkColor = ROOT.kViolet#+1 #+1
  SurfColor = ROOT.kGray+1 #+2
  NeckColor = ROOT.kCyan#+1


  ### Training, test events
  TrainingSetx = [i[0] for i in TrainingSet]
  TrainingSety = [i[1] for i in TrainingSet]
  TrainingSetr2 = [sqrt(x**2+y**2) for x,y in zip(TrainingSetx,TrainingSety)]
  TrainingSetz = [i[2] for i in TrainingSet]

  TestSetx = [i[0] for i in TestSet]
  TestSety = [i[1] for i in TestSet]
  TestSetr2 = [sqrt(x**2+y**2) for x,y in zip(TestSetx,TestSety)]
  TestSetz = [i[2] for i in TestSet]

  gTrainingSet = ROOT.TGraph(len(TrainingSet), -np.array(TrainingSetr2), np.array(TrainingSetz))
  gTrainingSet.GetXaxis().SetLimits(-R, R)
  gTrainingSet.GetYaxis().SetRangeUser(-R, R)
  gTrainingSet.SetMarkerSize(0.25)

  gTestSet = ROOT.TGraph(len(TestSet), np.array(TestSetr2), np.array(TestSetz))
  gTestSet.GetXaxis().SetLimits(-R, R)
  gTestSet.GetYaxis().SetRangeUser(-R, R)
  gTestSet.SetMarkerStyle(ROOT.kFullStar)
  gTestSet.SetMarkerSize(1)


  ### True test event class
  TestClass = []

  TestBulk = []
  TestBulkx = []
  TestBulky = []
  TestBulkr2 = []
  TestBulkz = []

  TestSurf = []
  TestSurfx = []
  TestSurfy = []
  TestSurfr2 = []
  TestSurfz = []

  TestNeck = []
  TestNeckx = []
  TestNecky = []
  TestNeckr2 = []
  TestNeckz = []

  for t in TestSet:
    TestClass.append(t[-1])
    if 'bulk' in t:
      TestBulk.append(t)
      TestBulkx.append(t[0])
      TestBulky.append(t[1])
      TestBulkr2.append(sqrt(t[0]**2+t[1]**2))
      TestBulkz.append(t[2])
    if 'surf' in t:
      TestSurf.append(t)
      TestSurfx.append(t[0])
      TestSurfy.append(t[1])
      TestSurfr2.append(sqrt(t[0]**2+t[1]**2))
      TestSurfz.append(t[2])
    if 'neck' in t:
      TestNeck.append(t)
      TestNeckx.append(t[0])
      TestNecky.append(t[1])
      TestNeckr2.append(sqrt(t[0]**2+t[1]**2))
      TestNeckz.append(t[2])

  gTestBulk = ROOT.TGraph(len(TestBulk), -np.array(TestBulkr2), np.array(TestBulkz))
  gTestBulk.GetXaxis().SetLimits(-R, R)
  gTestBulk.GetYaxis().SetRangeUser(-R, R)
  gTestBulk.SetMarkerStyle(ROOT.kFullStar)
  gTestBulk.SetMarkerColor(BulkColor)
  gTestBulk.SetMarkerSize(1)

  gTestSurf = ROOT.TGraph(len(TestSurf), -np.array(TestSurfr2), np.array(TestSurfz))
  gTestSurf.GetXaxis().SetLimits(-R, R)
  gTestSurf.GetYaxis().SetRangeUser(-R, R)
  gTestSurf.SetMarkerStyle(ROOT.kFullStar)
  gTestSurf.SetMarkerColor(SurfColor)
  gTestSurf.SetMarkerSize(1)

  gTestNeck = ROOT.TGraph(len(TestNeck), -np.array(TestNeckr2), np.array(TestNeckz))
  gTestNeck.GetXaxis().SetLimits(-R, R)
  gTestNeck.GetYaxis().SetRangeUser(-R, R)
  gTestNeck.SetMarkerStyle(ROOT.kFullStar)
  gTestNeck.SetMarkerColor(NeckColor)
  gTestNeck.SetMarkerSize(1)


  ### Predicted event class
  PredBulk = []
  PredBulkx = []
  PredBulky = []
  PredBulkr2 = []
  PredBulkz = []

  PredSurf = []
  PredSurfx = []
  PredSurfy = []
  PredSurfr2 = []
  PredSurfz = []

  PredNeck = []
  PredNeckx = []
  PredNecky = []
  PredNeckr2 = []
  PredNeckz = []

  for p,t in zip(Predictions,TestSet):
    if 'bulk' in p:
      PredBulk.append(t)
      PredBulkx.append(t[0])
      PredBulky.append(t[1])
      PredBulkr2.append(sqrt(t[0]**2+t[1]**2))
      PredBulkz.append(t[2])
    if 'surf' in p:
      PredSurf.append(t)
      PredSurfx.append(t[0])
      PredSurfy.append(t[1])
      PredSurfr2.append(sqrt(t[0]**2+t[1]**2))
      PredSurfz.append(t[2])
    if 'neck' in p:
      PredNeck.append(t)
      PredNeckx.append(t[0])
      PredNecky.append(t[1])
      PredNeckr2.append(sqrt(t[0]**2+t[1]**2))
      PredNeckz.append(t[2])

  gPredBulk = ROOT.TGraph(len(PredBulk), np.array(PredBulkr2), np.array(PredBulkz))
  gPredBulk.GetXaxis().SetLimits(-R, R)
  gPredBulk.GetYaxis().SetRangeUser(-R, R)
  gPredBulk.SetMarkerStyle(ROOT.kFullStar)
  gPredBulk.SetMarkerColor(BulkColor)
  gPredBulk.SetMarkerSize(1)

  gPredSurf = ROOT.TGraph(len(PredSurf), np.array(PredSurfr2), np.array(PredSurfz))
  gPredSurf.GetXaxis().SetLimits(-R, R)
  gPredSurf.GetYaxis().SetRangeUser(-R, R)
  gPredSurf.SetMarkerStyle(ROOT.kFullStar)
  gPredSurf.SetMarkerColor(SurfColor)
  gPredSurf.SetMarkerSize(1)

  gPredNeck = ROOT.TGraph(len(PredNeck), np.array(PredNeckr2), np.array(PredNeckz))
  gPredNeck.GetXaxis().SetLimits(-R, R)
  gPredNeck.GetYaxis().SetRangeUser(-R, R)
  gPredNeck.SetMarkerStyle(ROOT.kFullStar)
  gPredNeck.SetMarkerColor(NeckColor)
  gPredNeck.SetMarkerSize(1)


  ### (In)Correct
  Correctx = [i[0] for i in Correct]
  Correcty = [i[1] for i in Correct]
  Correctr2 = [sqrt(x**2+y**2) for x,y in zip(Correctx,Correcty)]
  Correctz = [i[2] for i in Correct]

  Incorrectx = [i[0] for i in Incorrect]
  Incorrecty = [i[1] for i in Incorrect]
  Incorrectr2 = [sqrt(x**2+y**2) for x,y in zip(Incorrectx,Incorrecty)]
  Incorrectz = [i[2] for i in Incorrect]

  gCorrect = ROOT.TGraph(len(Correct), -np.array(Correctr2), np.array(Correctz))
  gCorrect.GetXaxis().SetLimits(-R, R)
  gCorrect.GetYaxis().SetRangeUser(-R, R)
  gCorrect.SetMarkerStyle(ROOT.kFullStar)
  gCorrect.SetMarkerColor(ROOT.kGreen)
  gCorrect.SetMarkerSize(1)

  gIncorrect = ROOT.TGraph(len(Incorrect), np.array(Incorrectr2), np.array(Incorrectz))
  gIncorrect.GetXaxis().SetLimits(-R, R)
  gIncorrect.GetYaxis().SetRangeUser(-R, R)
  gIncorrect.SetMarkerStyle(ROOT.kFullStar)
  gIncorrect.SetMarkerColor(ROOT.kRed)
  gIncorrect.SetMarkerSize(1)


  ### Plot full dataset on one canvas
  c = ROOT.TCanvas('c', 'Toy DEAP', 1366, 550) #1366,720,455
  c.Divide(3,1)

  c.cd(1)
  ROOT.gPad.SetTopMargin(2)
  ROOT.gPad.SetRightMargin(0)
  gTestSet.SetTitle('Full: Training = '+str(len(TrainingSet))+', Test = '+str(len(TestSet))+';#sqrt{x^{2}+y^{2}};z')
  gTestSet.Draw('ap')
  gTrainingSet.Draw('same p')

  c.cd(2)
  ROOT.gPad.SetRightMargin(0)
  gTestBulk.SetTitle('#splitline{True:          Bulk = '+str(len(TestBulk))+', Surf = '+str(len(TestSurf))+', Neck = '+str(len(TestNeck))+'}{Prediction: Bulk = '+str(len(PredBulk))+', Surf = '+str(len(PredSurf))+', Neck = '+str(len(PredNeck))+', k = '+str(k)+'};;')
  gTestBulk.Draw('ap')
  gTestNeck.Draw('same p')
  gTestSurf.Draw('same p')
  gPredBulk.Draw('same p')
  gPredNeck.Draw('same p')
  gPredSurf.Draw('same p')

  c.cd(3)
  ROOT.gPad.SetRightMargin(0)
  gCorrect.SetTitle('Correct = '+str(len(Correct))+', Incorrect = '+str(len(Incorrect))+' ('+str(int(len(Correct)/round(len(Correct)+len(Incorrect))*100.))+'%);;')
  gCorrect.Draw('ap')
  gIncorrect.Draw('same p')


  ### Create 3x3 confusion matrix to show what classes were predicted as correct/incorrect
  '''
       (x,y)
  T  n [0,2][1,2][2,2]
  R  s [0,1][1,1][2,1]
  U  b [0,0][1,0][2,0]
  E      b    s    n
          PREDICTED
  '''

  c5 = ROOT.TCanvas('c5', 'c5', 920, 720)
  ROOT.gStyle.SetTitleOffset(0.8, 'x')
  ROOT.gStyle.SetTitleOffset(0.8, 'y')

  nClasses = 3
  s = 10*' '
  ss = 20*' '
  sss = 30*' '

  h2D = ROOT.TH2D('h2D', 'h2D', nClasses,0,nClasses, nClasses,0,nClasses)
  h2D.SetStats(0)
  h2D.SetTitle('Confusion Matrix;'+ss+'pBulk'+sss+'pSurf'+sss+'pNeck'+ss+';'+ss+'tBulk'+ss+'tSurf'+ss+'tNeck'+ss)

  h2D.GetXaxis().CenterTitle()
  h2D.GetYaxis().CenterTitle()

  h2D.GetXaxis().SetNdivisions(100)
  h2D.GetYaxis().SetNdivisions(100)
  c.SetGrid()

  for p,t in zip(Predictions,TestClass):
    if ('bulk' in p) and ('bulk' in t):  h2D.Fill(0,0, 100./len(PredBulk)) #TestBulk or PredBulk?
    if ('bulk' in p) and ('surf' in t):  h2D.Fill(0,1, 100./len(PredBulk))
    if ('bulk' in p) and ('neck' in t):  h2D.Fill(0,2, 100./len(PredBulk))
    if ('surf' in p) and ('bulk' in t):  h2D.Fill(1,0, 100./len(PredSurf))
    if ('surf' in p) and ('surf' in t):  h2D.Fill(1,1, 100./len(PredSurf))
    if ('surf' in p) and ('neck' in t):  h2D.Fill(1,2, 100./len(PredSurf))
    if ('neck' in p) and ('bulk' in t):  h2D.Fill(2,0, 100./len(PredNeck))
    if ('neck' in p) and ('surf' in t):  h2D.Fill(2,1, 100./len(PredNeck))
    if ('neck' in p) and ('neck' in t):  h2D.Fill(2,2, 100./len(PredNeck))

  # Scale histogram to make z axis equate to percent
  #h2D.Scale(100./len(Correct)) 

  h2D.Draw('colz text')

  print 'Captured '+str(len(PredBulk))+'/'+str(len(TestBulk))+' ('+str(round(len(PredBulk)/float(len(TestBulk))*100.,2))+'%) of bulk events with an accuracy of '+str(int(round(len(PredBulk)*h2D.Integral(0,1,0,1)/100.)))+'/'+str(len(PredBulk))+' ('+str(round(h2D.Integral(0,1,0,1),2))+'%).'
  print ''


  c.SaveAs('Geometric_set='+str(len(TrainingSet)+len(TestSet))+'_s='+str(round(Split,2))+'_k='+str(k)+'_m='+metric+'_w='+str(weightboolean)+'_bn='+str(cleanbulk)+'_test.png')
  #c5.SaveAs('Confusion_train='+str(len(TrainingSet))+'_test='+str(len(TestSet))+'_k='+str(k)+'_m='+metric+'_w='+str(weightboolean)+'_bn='+str(cleanbulk)+'_test.png')
  c5.SaveAs('Confusion_set='+str(len(TrainingSet)+len(TestSet))+'_s='+str(round(Split,2))+'_k='+str(k)+'_m='+metric+'_w='+str(weightboolean)+'_bn='+str(cleanbulk)+'_test.png')
  
  #return c, c5





def Progress(i,Set):
  #if (i*100./len(Set))%10 >= 9.9:
  #if (i/len(Set))%10 == 0:
  #if len(TestSet)/10
  #  print str(int(round((i*100./len(Set)))))+'% done'
  #if (i+1-len(TestSet))/10%10 == 0
  #  print 
  if i%(int(round(len(Set),-1)/10)) == 0:
    print str(i/(int(round(len(Set),-1)/10)))+'0% done'



def main(k,split,metric,weightboolean,cleanbulk):
  start = time.clock()

  # Prepare data
  TrainingSet, TestSet = LoadData(Filename, Split)
  print ''
  print 'Data:', Filename
  print 'Features:', nFeatures
  print 'Data set:', len(TrainingSet+TestSet)
  print 'Split:', round(Split,2)
  print 'Training set:', len(TrainingSet)
  print 'Test set:', len(TestSet)
  print 'k:', k
  print 'Metric:', metric
  print 'Weight:', weightboolean
  print 'Clean bulk:', str(cleanbulk)+'%'

  # Generate predictions
  #ti = 1
  Predictions = []
  for i,testinstance in enumerate(TestSet):

    '''
    #print ti,'/',len(TestSet)
    if (ti*100./len(TestSet))%10 >= 9.9:
      print str(int(round((ti*100./len(TestSet)))))+'% done'
    '''
    Progress(i,TestSet)

    Neighbors, Distances = GetNeighbors(TrainingSet, testinstance, k, metric)

    # For now, only cleanbulk while nonweighted
    '''
    if weightboolean == 0:
      if cleanbulk == 0:
        Response = GetResponse(Neighbors)
      else:
        Response = GetResponseCleanBulk(Neighbors,cleanbulk)
    else:
        Weights  = GetWeights(Distances)
        Response = GetResponseWeighted(Neighbors, Weights)
    '''
    if cleanbulk == 0:
      Response = GetResponse(Neighbors,weightboolean,Distances)
    else:
      Response = GetResponseCleanBulk(Neighbors,cleanbulk)

    Predictions.append(Response)
    #ti += 1

  Accuracy, Correct, Incorrect = GetAccuracy(TestSet, Predictions)
  print('Accuracy: ' + repr(round(Accuracy,2)) + '%')

  stop = time.clock()
  print 'Runtime:', stop-start, 's'
  print ''

  #Geometric, Confusion = 
  Plot(TrainingSet, TestSet, Predictions, Correct, Incorrect, k, metric, weightboolean, cleanbulk)

  #return TrainingSet, TestSet, Predictions, Correct, Incorrect
  #return Geometric, Confusion











### Actually run program. I modified it to loop over values of k (, split).
# 67% of data to train model, last 33% to test model. 
Split = 2/3.

# k-nearest neighbors. How many nearest neighbors?
# From https://stackoverflow.com/questions/18110951/how-to-determine-k-value-for-the-k-nearest-neighbours-algorithm-for-a-matrix-in
if 'ToyDEAP' in Filename:
  nInstances = int(Filename.split('_')[1].split('-')[0])
if 'iris' in Filename:
  nInstances = 150.
k = int(round(sqrt(nInstances))/2)
# Force odd k
if k%2 == 0: k+=1

### Which distance metric?
MetricSet = ['e','m'] #['Euclidean', 'Manhattan']
Metric = MetricSet[0]

# Use weighted distance vote?
WeightBoolean = 1

# "Clean up" bulk? What percent of neighbors also 'bulk' to be predictd 'bulk'?
CleanBulkSet = [0,50,80,90,100]
CleanBulk = CleanBulkSet[0]




# How many times to loop over main()?
'''
Loops = 1
for m in xrange(Loops):
  TrainingSet, TestSet, Predictions, Correct, Incorrect = main(k,Split,WeightBoolean,CleanBulk)
'''
'''
for i in CleanBulkSet:
  main(k,Split,WeightBoolean,i)
'''
#Geometric, Confusion = 
main(k,Split,Metric,WeightBoolean,CleanBulk)




#import profile  
#profile.run('main()')
