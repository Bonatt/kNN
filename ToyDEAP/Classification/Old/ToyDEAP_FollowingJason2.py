import numpy as np
import random
import time
from collections import Counter


# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return np.sqrt(x)
# Redefine pi to something shorter
pi = np.pi


### From http://machinelearningmastery.com/tutorial-to-implement-k-nearest-neighbors-in-python-from-scratch/
# Edited to take ToyDEAP3600 data, which I put to file in same manner as iris.data.
# Takes data in the form [0.0763758342428, -0.377705472767, bulk] where nDatacolumns is 2 here. Third is event ID.




### Define data file, number of features, and data delimiter. Assumes n cols of feature doubles with n+1 col as classification string.
Filename = 'ToyDEAP.data'
Delimiter = ','

### Use some percent of data if desired, e.g. 100/100. == 100% of data.
PercentOfData = 10/100.

### 67% of data to train model, last 33% to test model. 
Split = 2/3.

### k-nearest neighbors. How many nearest neighbors?
#k = 3




### Load data, splitting data randomly into training or test sets.
def LoadData(filename, split, trainingset=[], testset=[]):
  data = np.genfromtxt(filename, delimiter=Delimiter, dtype=None)
  print 'Full dataset:', len(data)
  
  data = data[:int(len(data)*PercentOfData)]
  print 'Analyzed dataset:', len(data)

  # If data is not shuffled, do this:
  '''
  for instance in data:
    if random.random() < split:
      trainingset.append(instance)
    else:
      testset.append(instance)
  '''
  # Otherwise, just do this. Even shuffle to randomize too!
  random.shuffle(data)
  trainingset.extend(data[:int(len(data)*split)])
  testset.extend(data[int(len(data)*split):])

  # Or... From https://stackoverflow.com/questions/6482889/get-random-sample-from-list-while-maintaining-ordering-of-items
  #[ a[i] for i in sorted(random.sample(range(len(a)), len(a))) ]
  #[ a[i] for i in random.sample(range(len(a)), len(a)) ]

  print 'Test set:', len(testset)
  print 'Training set:', len(trainingset)
 

### Define Euclidean distance This distance-calculating method may vary (Manhattan, Hamming, ...), so define explicitly as such.
# r = sqrt( (x-x0)**2 + ... ) for 1D. Add (y-y0)**2 for 2D, etc.
def EuclideanDistance(instance1, instance2):
  features1 = list(instance1)[:-1]
  features2 = list(instance2)[:-1]
  r = sqrt(sum([(f1-f2)**2 for f1,f2 in zip(features1, features2)]))
  return r


### Determine k neighbors. Take in full training dataset and single test instance. 
# Compute distances between single test instance and every training instance (this is looped from main() for every test instance).
# Take k smallest distances.
def GetNeighbors(trainingset, testinstance, k):
  distances = []
  for traininginstance in trainingset:
    distance = EuclideanDistance(traininginstance, testinstance)
    distances.append( (traininginstance, distance) )

  # distances has form ((-0.354481106014, -0.784429584872, 'bulk'), 0.611469612650144) = ((x,y,id),r). 
  # Sort by r. Take only first k neighbors. Then take only k neighbors without attached distance.
  neighbors_distances = sorted(distances, key=lambda r:r[:][-1])[:k]
  neighbors = [i[0] for i in neighbors_distances]
  return neighbors


### Get response, i.e. vote. Assume classification is index [-1] of neighbors. Counts how many times each classification appears.
def GetResponse(neighbors):
  responses = [i[-1] for i in neighbors]
  response = Counter(responses).most_common(1)[0][0]
  return response


### How accurate was response? This could be done in above loop...
def GetAccuracy(testset, predictions):
  correct = 0
  for id,p in zip(testset, predictions):
    if id[-1] == p:
      correct = correct + 1
  accuracy = correct/float(len(testset))*100.
  return accuracy



def sortby(somelist, n):
    nlist = [(x[n], x) for x in somelist]
    nlist.sort()
    return [val for (key, val) in nlist]





### I think the primary benefit of putting all "running" code under a main() function is so one can import without "running".
# However, these variables are not saved outside, and cannot be called later for troubleshotting.
def main(k,split):
  start = time.clock()

  print ''
  print 'Data:', Filename
  print 'Percent of data analyzed:', str(int(PercentOfData*100))+'%'
  print 'Split:', round(split,2)
  print 'k:', k

  TrainingSet = []
  TestSet = []
  LoadData(Filename, split, TrainingSet, TestSet)

  Predictions = []
  for testinstance in TestSet:
    Neighbors = GetNeighbors(TrainingSet, testinstance, k)
    Responses = GetResponse(Neighbors)
    Predictions.append(Responses)

  Accuracy = GetAccuracy(TestSet, Predictions)
  print 'Accuracy:', str(round(Accuracy,2))+'%'

  end = time.clock()
  print 'Time:', end-start, 's'


### Actually run program. I modified it to loop over values of k (, split).
Loops = 1
for k in range(1,Loops+1):
  main(k,Split)
